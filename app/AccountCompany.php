<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountCompany extends Model
{
    
    protected $table = "accountcompany";
    
    public $timestamps = false;

    protected $fillable = [
        'AccountID', 
        'CompanyID',
    ];
}
