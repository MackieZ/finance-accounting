<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionLine extends Model
{
    protected $table = "transactionline";
    
    public $timestamps = false;

    protected $fillable = [
        'TransactionID', 
        'TransactionLineNumber',
        'Debit',
        'Credit',
        'AccountID'
    ];
}
