<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountStaff extends Model
{
    //
    protected $table = "accountstaff";
    
        public $timestamps = false;
    
        protected $fillable = [
            'AccountID',
            'StaffID', 
            
        ];
}
