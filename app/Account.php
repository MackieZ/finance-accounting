<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    ///
    protected $table = "account";
    
    public $timestamps = false;

    protected $fillable = [
        'AccountID', 
        'AccountName',
        'AccountType',
    ];
}
