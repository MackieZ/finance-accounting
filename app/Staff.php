<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    //
    protected $table = "staff";

    public $timestamps = false;

    protected $fillable = [
        'StaffID', 
        'StaffFirstName',
        'StaffLastName',
        'StaffPhoneNumber',
        'StaffAddress',
        'DepartmentID'
    ];

   
}
