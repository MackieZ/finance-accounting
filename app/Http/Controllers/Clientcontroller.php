<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class Clientcontroller extends Controller
{
    public function form ()
    {
        return view('client');
    }

    public function cancel ()
    {
        return back();
    }

    public function submit (Request $request)
    
    { 
        $clname = $request->clname;
        $clphone = $request->clphone;
        $clmail= $request->clmail;
        DB::table('client')->insert([
            'ClientName' => $clname,
            'PhoneNumber' => $clphone,
            'Email' => $clmail

            ]);

        return back();
        
    }
}
