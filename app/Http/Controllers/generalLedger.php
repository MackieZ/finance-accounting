<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class generalLedger extends Controller
{
    //
    public function home(){
        $query=DB::table('account')
        ->get();
        $i=0;
        foreach($query as $value){
            $account[$i]=$value;
            $i++;
        }
        #dd($account);
        return view('generalLedger',compact('account'));
    }

    public function getGeneralLedger(Request $request){
        $start = $request->start;
        $end = $request->end;
        $accountNo = $request->no;
        $data=self::query($start,$end,$accountNo);
        $balance = 0;
        $i=0;
        $td=0;
        $tc=0;
        foreach($data as $value){
            $balance += $value->Debit - $value->Credit;
            $ledger[$i]=$value;
            $ledger[$i]->balance=$balance;
            $td+=$value->Debit;
            $tc+=$value->Credit;
            $i++;
        }
        $account = self::account($accountNo);
        return view('viewGeneralLedger',compact('ledger','account','start','end','td','tc'));

    }

    function query($start,$end,$accountNo){
        $query['query']=DB::table('account')
        ->join('transactionline','transactionline.accountID','=','account.accountID')
        ->join('transaction','transaction.transactionID','=','transactionline.transactionID')
        ->join('invoicetransaction','transaction.transactionID','=','invoicetransaction.transactionID')
        ->join('invoice','invoice.invoiceID','=','invoicetransaction.invoiceID')
        ->join('journal','journal.journalID','=','transaction.journalID')
        ->where('account.accountID','=',$accountNo)
        ->where('invoice.invoiceDate','>',$start)
        ->where('invoice.invoiceDate','<',$end)
        ->orderby('invoice.invoiceDate')
        ->get();
        foreach($query as $value){
            $unroll=$value;
        }
        return $unroll;

    }

    function account($no){
        $account = DB::table('account')
        ->where('accountID','=',$no)
        ->get();
        foreach($account as $value){
                $unroll = $value;
        }
        return $unroll;
    }

}
