<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class editstaffController extends Controller
{
    public function form ()
    
    {   $departmentname=DB::table('department')->get(); 
        $companyname=DB::table('company')->get(); 
        $accountname=DB::table('account')->get();
        $allstaff=DB::table('staff')->get();
        return view('editstaffcom',compact('companyname','departmentname','accountname','allstaff'));
    }
    public function cancel ()
    {
        return back();
    }
    public function report (Request $request)
    {    $sid= $request->sid;
        $thisstaff = DB::table('staff')->where('StaffID', '=', $sid)->get();
        //dd($thisstaff);
        $allstaff=DB::table('staff')->get();
        $departmentname=DB::table('department')->get(); 
        $companyname=DB::table('company')->get(); 
        $accountname=DB::table('account')->get();
        return view('editstaff',compact('companyname','departmentname','accountname','allstaff','thisstaff','sid'));
    }

    public function putdata (Request $request)
    
    {   
    $tid = $request->tid ;
    $stfirstname = $request->stfirstname;
    $stlastname = $request->stlastname;
    $staddr = $request->staddr;
    $stphone = $request->stphone;
    $did = $request->did;
        $aid = $request->aid;
        $cid = $request->cid;
     DB::table('staff')->where('StaffID','=',$tid)->update([
           'StaffFirstName' => $stfirstname,
           'StaffLastName' => $stlastname,
           'StaffPhoneNumber' => $stphone,
           'StaffAddress' => $staddr,
           'DepartmentID' => $did,
           'AccountID' => $aid,
           'CompanyID' => $cid
           ]); 
       //DB::table('company')->insertGetId([])
       return back();
        
    }
}
