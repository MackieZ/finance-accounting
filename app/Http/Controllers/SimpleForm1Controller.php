<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class SimpleForm1Controller extends Controller
{
    
    //
    protected $data = [
        '123',
        '456'
    ];
    public function home ()
    {
        return view('simple-form1',['data' => $this->data]);
    }

    public function addForm (Request $request)
    {
        $body = $request->company;

        DB::table('company')->insert(['CompanyName'=>$body]);
        // return back()->with('datas','123');
        // return view('simple-form1',['data' => [$body]]);
        return redirect()->back()->with('inserted',$body);
        
    }
}
