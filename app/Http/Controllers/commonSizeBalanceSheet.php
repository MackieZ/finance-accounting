<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class commonSizeBalanceSheet extends Controller
{
    //
    public function home(){
        $asset = self::query('Assets');
        $liability = self::query('Liability');
        $equity = self::query('Equity');
        $totalAsset = self::totalDebit('Assets');
        $totalLiability = self::totalDebit('Liability');
        $totalEquity = self::totalDebit('Equity');
        $totalCredit = $totalLiability+$totalEquity;
        $i=0;
        foreach($asset as $value){
            $asset[$i]->p=$value->debit/$totalAsset*100;
            $i++;
        }
        $i=0;
        foreach($equity as $value){
            $equity[$i]->p=$value->debit/$totalCredit*100;
            $i++;
        }
        $i=0;
        foreach($liability as $value){
            $liability[$i]->p=$value->debit/$totalCredit*100;
            $i++;
        }
        return view('commonSizeBalanceSheet',compact('asset','liability','equity','totalAsset','totalCredit'));
    }


    public function query($type){
        $query['query'] = DB::table('account')
        ->join('transactionline','account.accountID','=','transactionline.accountID')
        ->select(DB::raw('account.accountName as name,SUM(transactionline.Debit-transactionline.Credit) as debit'))
        ->where('account.accountType','like',$type)
        ->groupBy('account.accountName')
        ->get();
        $i=0;
        // dd($query);
        foreach($query as $value){
            foreach($value as $value2){
                $unroll[$i] = $value2;
                $i++;
            }
        }
        return $unroll;
    }

    public function totalDebit($type){
        $query['query'] = DB::table('account')
        ->join('transactionline','account.accountID','=','transactionline.accountID')
        ->select(DB::raw('SUM(transactionline.Debit-transactionline.Credit) as debit'))
        ->where('account.accountType','like',$type)
        ->get();
        foreach($query as $value){
            foreach($value as $value2)
                $debit=$value2->debit;
        }
        return $debit;
    }


}
