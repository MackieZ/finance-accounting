<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Staff;
use App\AccountStaff;
use App\AccountCompany;
use App\Account;
use App\Company;
use DB;

class AccountComplexController extends Controller
{
    public function form ()
    {
        $list = DB::table('account')->get();
        
        return view('account-complex',['listAccount' => $list]);
    }

    public function cancel ()
    {
        return back();
    }

    public function edit ($id)
    {
        $data =  DB::table('account')     
            -> join('accountstaff', 'account.AccountID','accountstaff.AccountID')
            -> join('staff', 'accountstaff.StaffID','staff.StaffID')
            -> select('account.AccountID',
                'account.AccountName',
                'account.AccountType',
                'accountstaff.StaffID',
                'staff.StaffFirstName',
                'staff.StaffLastName')
            -> where('account.AccountID', $id)
            -> first();

        $accountcompany = DB::table('account')
            -> join('accountcompany', 'account.AccountID', 'accountcompany.AccountID')
            -> join('company', 'accountcompany.CompanyID', 'company.CompanyID')
            -> where('account.AccountID', $id)
            -> get();

        $staff = DB::table('staff')
            -> get();

        $company = DB::table('company')
            -> get();
        // dd($accountcompany);
        return view('account-complex-edit',[
                'data' => $data,
                'staff' => $staff,
                'company' => $company,
                'accountcompany' => $accountcompany,
            ]);    
    }

    public function editSave (Request $request)
    {    
        $account = Account::where('AccountID', $request -> accId)
        -> update(['AccountName' => $request -> accountname, 'AccountType' => $request -> accounttype]);
        $staffId = Staff::where('StaffFirstName', $request -> sel1) 
        -> first();
        $accountstaff = AccountStaff::where('AccountID', $request -> accId)
        -> update(['StaffID' => $staffId -> StaffID]);
        
        $accountcompany = AccountCompany::where('AccountID', $request -> accId)
        -> delete();
        
        foreach($request->sel2 as $selcompany){
            $company = Company::where('CompanyName', $selcompany)
                -> first();
            $UpdateAccount = new AccountCompany;
            $UpdateAccount -> AccountID = $request -> accId;
            $UpdateAccount -> CompanyID = $company -> CompanyID;
            $UpdateAccount -> save();
        }
        
        return redirect("/");
    }
}
