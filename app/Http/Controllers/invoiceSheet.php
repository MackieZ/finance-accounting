<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class invoiceSheet extends Controller
{
    //
    public function home(){
        return view('invoiceSheet');
    }

    public function getInvoiceSheet(Request $request){
        $date = $request->date;
        $clientID = $request->clientID;
        $invoiceSheet = self::query($date,$clientID);
        $i=0;
        $total=0;
        foreach($invoiceSheet as $value){
            $invoice[$i] = $value;
            $total+=$value->Total;
            $i++;
        }
        $rolldate = DB::table('invoice')
        ->select('invoiceDate')
        ->where('invoiceID','=',$clientID)
        ->get();
        foreach($rolldate as $value){
            $date=$value->invoiceDate;
        }
        $rollclientName = DB::table('invoice')
        ->join('client','invoice.clientID','=','client.clientID')
        ->where('invoiceID','=',$clientID)
        ->get();
        foreach($rollclientName as $value){
        
                $clientName = $value->ClientName;
        }
        return view('viewInvoiceSheet',compact('invoice','total','date','clientName'));
    }

    function query($date,$client){
        $query = DB::table('invoice')
        ->join('client','invoice.clientID','=','client.clientID')
        ->join('invoiceLine','invoice.invoiceID','=','invoiceLine.invoiceID')
        ->join('product','product.productID','=','invoiceline.productID')
        ->select(DB::raw('product.ProductName AS ProductName,
        Product.ProductDescription As Descriptioin,
        product.ProductPrice As Price,
        invoiceLine.Amount as Amount,
        invoiceLine.Amount*product.productPrice As Total'))
        #->where('invoice.invoiceDate','=',$date)
        ->where('invoice.invoiceID','=',$client)
        ->get();
        #dd($query);
        return $query;
    }

}
