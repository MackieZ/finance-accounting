<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class Accountcontroller extends Controller
{
   
        public function form ()
        {
            return view('account');
        }
    
        public function cancel ()
        {
            return back();
        }
    
        public function submit (Request $request)
        
        { 
            $accountname = $request->accountname;
            $accounttype = $request->accounttype;
            DB::table('account')->insert([
                'AccountName' => $accountname,
                'AccountType' => $accounttype

                ]);
    
            return back();
            
        }
   
}
