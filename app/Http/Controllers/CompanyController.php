<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function form ()
    {
        return view('company');
    }

    public function cancel ()
    {
        return back();
    }

    public function submit (Request $request)
    
    { 
        $companyname = $request->companyname;
        DB::table('company')->insert([
            'CompanyName' => $companyname
            ]);

        return back();
        
    }
}
