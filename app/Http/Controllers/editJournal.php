<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class editJournal extends Controller
{
    //

    public function home(){
        $journal = self::queryjournal();
        return view('editJournal',compact('journal'));
    }
    function queryjournal(){
        $query = DB::table('journal')
        ->get();
        $i=0;
        foreach($query as $value){
            $unroll[$i]=$value;
            $i++;
        }
        return $unroll;
    }
    public function getJournal(Request $request){
        $submit = $request->submit;
        $journalID = (int)$request->JournalID;
        if($submit =="2"){
            $query = self::queryAll($journalID);
            DB::table('journal')
            ->where('journalID','=',$journalID)
            ->delete();
            DB::table('transaction')
            ->where('journalID','=',$journalID)
            ->delete();
            DB::table('InvoiceTransaction')
            ->where('TransactionID','=',$query[0]->TransactionID)
            ->delete();
            DB::table('journalcompany')
            ->where('journalID','=',$journalID)
            ->delete();
            foreach($query as $data){
                DB::table('transactionline')
                ->where('TransactionID','=',$data->TransactionID)
                ->delete();
            }
            return view('main');
        }
        elseif($submit =="1"){
            $journal = self::queryAll($journalID);
            $transaction = self::queryTransaction($journalID);
            

            $rollcompany = DB::table('company')
            ->get();
            $i=0;
            foreach($rollcompany as $value){
                $company[$i] = $value;
                $i++;
            }
            $transactionline = self::queryTransactionLine($transaction->TransactionID);
            $account = self::getAccount();
            return view('editTransaction',compact('journal','company','transaction','transactionline','account'));
        }

    }
    function transactionlineEdit(Request $request){
        $transactionID=$request->transactionID;
        $transactionlineID=$request->transactioinlineID;
        $account=(int)$request->account;
        $debit=(int)$request->debit;
        $credit=(int)$request->credit;

        DB::table('transactionline')
        ->where('transactionID','=',$transactionID)
        ->where('transactionlineNumber','=',$transactionlineID)
        ->update(['accountID'=>$account,'Debit'=>$debit,'Credit'=>$credit]);
        echo "form edited";
        echo "<br>";
        echo "<a href=\"javascript:history.go(-1)\">GO BACK</a>";
        

    }
    function getAccount(){
        $query = DB::table('Account')
        ->get();
        $i=0;
        foreach($query as $value){
            $unroll[$i]= $value;
            $i++;
        }
        return $unroll;
    }
    public function companyEdit(Request $request){
        $companyID = (int)$request->companyID;
        $transactionID=(int)$request->transactionID;
        DB::table('transaction')
        ->where('transactionID','=',$transactionID)
        ->update(['companyID'=>$companyID]);

        echo "form edited";
        echo "<br>";
        echo "<a href=\"javascript:history.go(-1)\">GO BACK</a>";
        
    }

    public function invoiceEdit(Request $request){
        $invoiceID=(int)$request->invoiceID;
        $date = $request->date;
        DB::table('invoice')
        ->where('invoiceID','=',$invoiceID)
        ->update(['invoiceDate'=>$date]);
        echo "form edited";
        echo "<br>";
        echo "<a href=\"javascript:history.go(-1)\">GO BACK</a>";
    }

    public function journalEdit(Request $request){
        $journalID = (int)$request->journalID;
        $journalName = $request->newJournal;
        $description = $request->newDescription;
        DB::table('journal')
        ->where('journalID','=',$journalID)
        ->update(['journalName'=>$journalName,'JournalDescription'=>$description]);
         
        echo "form edited";
        echo "<br>";
        echo "<a href=\"javascript:history.go(-1)\">GO BACK</a>";
    }

    function queryTransactionLine($transactionID){
        $query = DB::table('transactionline')
        ->where('transactionID','=',$transactionID)
        ->get();
        $i=0;
        foreach($query as $data){
            $unroll[$i]=$data;
            $i++;
        }
        return $unroll;
        
    }

    function queryTransaction($journalID){
        $query = DB::table('transaction')
        ->join('invoicetransaction','transaction.transactionID','=','invoicetransaction.transactionID')
        ->join('invoice','invoicetransaction.invoiceID','=','invoice.invoiceID')
        ->where('transaction.journalID','=',$journalID)
        ->get();
        foreach($query as $data){
            $unroll = $data;
        }
       
        return $unroll;
    }

    function queryAll($journalID){
        $query = DB::table('journal')
        ->join('transaction','transaction.journalID','=','journal.journalID')
        ->where('journal.journalID','=',$journalID)
        ->get();
        $i=0;
        foreach($query as $value){
            $unroll[$i]=$value;
            $i++;
        }
        return $unroll;
    }


}
