<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class addTransaction extends Controller
{
    //

    public function home(){
        return view('addTransaction');
    }

    public function getAddTransaction(Request $request){
        $journalid = $request->journalid;
        $companyid = $request->companyid;
        $invoiceid = $request->invoiceid;
        $invoicedate = $request->invoicedate;
        $clientid = $request->clientid;
        $paymenttype = $request->paymenttype;
        $mode = 'add';
        $id = DB::table('transaction')->insertGetId([
            'journalid' => $request->journalid,
            'companyid' => $request->companyid
            ]);
        if($invoiceid){
            DB::table('invoicetransaction')->insert([
                'invoiceid' => $request->invoiceid,
                'transactionid' => $id
            ]);
            DB::table('invoice')->insert([
                'invoiceid' => $request->invoiceid,
                'invoicedate' => $request->invoicedate,
                'clientid' => $request->clientid,
                'paymenttype' => $request->paymenttype
            ]);
        }
        return view('viewAddTransaction', compact('mode'));         
    }

    public function getEditTransaction(Request $request){
        $transactionid = $request->transactionid;
        $journalid = $request->journalid;
        $companyid = $request->companyid;
        $mode = 'edit';
        DB::table('transaction')
        -> where('transactionid', $transactionid)
        -> update(array('journalid'=>$journalid,'companyid'=>$companyid));   
        return view('viewAddTransaction', compact('mode'));         
    }

    public function getDeleteTransaction(Request $request){
        $transactionid = $request->transactionid;
        $mode = 'delete';
        DB::table('transaction')
        ->where('transactionid', '=', $transactionid)
        ->delete(); 
        
        DB::table('invoicetransaction')
        ->where('transactionid', '=', $transactionid)
        ->delete(); 
        
        return view('viewAddTransaction', compact('mode'));   
    }
}
