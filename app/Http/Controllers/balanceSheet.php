<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Account;

class balanceSheet extends Controller
{
    //
   
    function getBalanceSheet(){
        $asset['asset'] = DB::table('account')
        ->join('transactionline','account.accountID','=','transactionline.accountID')
        ->select(DB::raw('account.accountName as name,SUM(transactionline.Debit-transactionline.Credit) as debit'))
        ->where('account.accountType','like',"assets")
        ->groupBy('account.accountName')
        ->get();

        
        $liability['liability'] = DB::table('account')
        ->join('transactionline','account.accountID','=','transactionline.accountID')
        ->select(DB::raw('account.accountName as name,SUM(transactionline.Debit-transactionline.Credit) as debit'))
        ->where('account.accountType','like',"liability")
        ->groupBy('account.accountName')
        ->get();

        $equity['equity'] = DB::table('account')
        ->join('transactionline','account.accountID','=','transactionline.accountID')
        ->select(DB::raw('account.accountName as name,SUM(transactionline.Debit-transactionline.Credit) as debit'))
        ->where('account.accountType','like',"equity")
        ->groupBy('account.accountName')
        ->get();

        $totalAsset['totalAsset'] = DB::table('account')
        ->join('transactionline','account.accountID','=','transactionline.accountID')
        ->select(DB::raw('SUM(transactionline.Debit-transactionline.Credit) as debit'))
        ->where('account.accountType','like','assets')
        ->get();

        foreach($totalAsset as $value){
            foreach($value as $value2)
                $ta=$value2->debit;
        }

        $totalLiability['totalLiability'] = DB::table('account')
        ->join('transactionline','account.accountID','=','transactionline.accountID')
        ->select(DB::raw('SUM(transactionline.Debit-transactionline.Credit) as debit'))
        ->where('account.accountType','like','liability')
        ->get();
        
        foreach($totalLiability as $value){
            foreach($value as $value2)
                $tl=$value2->debit;
        }

        $totalEquity['totalEquity'] = DB::table('account')
        ->join('transactionline','account.accountID','=','transactionline.accountID')
        ->select(DB::raw('SUM(transactionline.Debit-transactionline.Credit) as debit'))
        ->Where('account.accountType','like','equity')
        ->get();

        foreach($totalEquity as $value){
            foreach($value as $value2)
                $te=$value2->debit;
        }

        $total=$ta+$tl+$te;
        return view('balanceSheet',compact('asset','liability','equity','ta','tl','te','total'));
    }
    function getTotal()
    {
        $asset['asset'] = DB::table('account')
        ->join('transactionline','account.accountID','=','transactionline.accountID')
        ->select(DB::raw('account.accountName as name,SUM(transactionline.Debit-transactionline.Credit) as debit'))
        ->where('account.accountType','like',"assets")
        ->groupBy('account.accountName')
        ->get();

        
        $liability['liability'] = DB::table('account')
        ->join('transactionline','account.accountID','=','transactionline.accountID')
        ->select(DB::raw('account.accountName as name,SUM(transactionline.Debit-transactionline.Credit) as debit'))
        ->where('account.accountType','like',"liability")
        ->groupBy('account.accountName')
        ->get();

        $equity['equity'] = DB::table('account')
        ->join('transactionline','account.accountID','=','transactionline.accountID')
        ->select(DB::raw('account.accountName as name,SUM(transactionline.Debit-transactionline.Credit) as debit'))
        ->where('account.accountType','like',"equity")
        ->groupBy('account.accountName')
        ->get();

        $totalAsset['totalAsset'] = DB::table('account')
        ->join('transactionline','account.accountID','=','transactionline.accountID')
        ->select(DB::raw('SUM(transactionline.Debit-transactionline.Credit) as debit'))
        ->where('account.accountType','like','assets')
        ->get();

        foreach($totalAsset as $value){
            foreach($value as $value2)
                $ta=$value2->debit;
        }

        $totalLiability['totalLiability'] = DB::table('account')
        ->join('transactionline','account.accountID','=','transactionline.accountID')
        ->select(DB::raw('SUM(transactionline.Debit-transactionline.Credit) as debit'))
        ->where('account.accountType','like','liability')
        ->get();
        
        foreach($totalLiability as $value){
            foreach($value as $value2)
                $tl=$value2->debit;
        }

        $totalEquity['totalEquity'] = DB::table('account')
        ->join('transactionline','account.accountID','=','transactionline.accountID')
        ->select(DB::raw('SUM(transactionline.Debit-transactionline.Credit) as debit'))
        ->Where('account.accountType','like','equity')
        ->get();

        foreach($totalEquity as $value){
            foreach($value as $value2)
                $te=$value2->debit;
        }

        $total=$ta+$tl+$te;
        //$total
        return   response()->json($total);
    }

    function getTotalAccount(){
        return response()->json(Account::count());
    }
    function getIncomeStatement(){
        return view('incomeStatement');
    }

}
