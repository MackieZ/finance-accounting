<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class generalJournal extends Controller
{
    //
    public function home(){
        return view('/generalJournal');
    }

    public function getGeneralJournal(Request $request){
        $start = $request->start;
        $end = $request->end;
        $rollinvoice = self::getinvoice($start,$end);
        $i=0;
        foreach($rollinvoice as $value){
            $inv =$value;
        }
       
        foreach($inv as $value){

           $invoice[$i]= $value;
           $id = $invoice[$i]->InvoiceID;
           $invoice[$i]->data = self::query($id);
           $i++;
        }
        
        return view('viewGeneralJournal',compact('start','end','invoice'));

    }

    function getInvoice($start,$end){
        $invoice['invoice']=DB::table('invoice')
        ->where('invoiceDate','>=',$start)
        ->where('invoiceDate','<=',$end)
        ->where('paymentType','!=',"0")
        ->get();

        return $invoice;
    }


    function query($invoiceID){
        $query['query']=DB::table('transaction')
        ->join('invoicetransaction','invoicetransaction.transactionID','=','transaction.transactionID')
        ->join('transactionline','transactionline.transactionID','=','transaction.transactionID')
        ->join('account','account.accountID','=','transactionline.accountID')
        ->join('company','company.CompanyID','=','transaction.CompanyID')
        #->where('company.CompanyID','=','0')
        ->where('invoicetransaction.invoiceID','=',$invoiceID)
        ->get();

        #dd($query);
        foreach($query as $value){
            $unroll = $value;
        }

        return $unroll  ;
    }
}
