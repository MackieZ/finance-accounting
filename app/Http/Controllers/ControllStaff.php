<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class ControllStaff extends Controller
{
    public function form ()
    {
        return view('Staff');
    }

    public function cancel ()
    {
        return back();
    }

    public function submit (Request $request)
    
    {   $stfirstname = $request->stfirstname;
        $stlastname = $request->stlastname;
        $staddress = $request->staddress;
        $stphone = $request->stphone;
        $stdepid = $request->stdepid;
        $staccid = $request->staccid;
        $stcom = $request->stcom;
        DB::table('staff')->insert([
            'StaffFirstName' => $stfirstname,
            'StaffLastName' => $stlastname,
            'StaffPhoneNumber' => $stphone,
            'StaffAddress' => $staddress,
            'DepartmentID' => $stdepid,
            'AccountID' => $staccid,
            'CompanyID' => $stcom
            ]);
        return back();
        
    }

   
}
