<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function form ()
    {
        return view('product');
    }

    public function cancel ()
    {
        return back();
    }

    public function submit (Request $request)
    {
        $productName = $request->productName;
        $productDescription = $request->productDescription;
        $productPrice = $request->productPrice;
        $productQuantity = $request->productQuantity;
        DB::table('product')->insert([
            'ProductName' => $productName,
            'ProductDescription' => $productDescription,
            'ProductPrice' => $productPrice,
            'ProductQuantity' => $productQuantity
            ]);

        return back();
        
    }

   
}
