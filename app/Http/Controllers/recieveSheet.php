<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class recieveSheet extends Controller
{
    //
    public function home(){
        return view('recieveSheet');
    }

    public function getRecieveSheet(Request $request){
        $start = $request->seart;
        $end = $request->end;

        $recieve=self::queryC($start,$end);
        $i=0;
        foreach($recieve as $value){
                $data[$i] = self::queryD($value->TransactionID,$value->Credit);
                $i++;
        }
        $i=0;
        $j=0;
        foreach($data as $value){
            if(!$value->isEmpty()){
                foreach($value as $value2){
                    $value3[$i]=$value2;
                    $value3[$i]->pay = $recieve[$j];
                }
                $i++;
            }
            $j++;
        }
        return view('viewRecieveSheet',compact('start','end','value3'));

    }

    public function queryC($start,$end){
        $recieveSheet['recieveSheet'] = DB::table('transaction')
        ->join('transactionline','transaction.transactionID','=','transactionline.transactionID')
        ->join('account','transactionline.accountID','account.accountID')
        ->join('invoiceTransaction','transaction.transactionID','=','invoiceTransaction.transactionID')
        ->join('invoice','invoice.invoiceID','=','invoiceTransaction.invoiceID')
        ->where('transactionline.credit','>','0')
        ->get();

        foreach($recieveSheet as $value){
            $unroll = $value;
        }
        
        return $unroll;
    }

    public function queryD($transactionID,$credit){
        $recieveSheet['recieveSheet'] = DB::table('transactionline')
        ->join('account','account.accountID','=','transactionline.accountID')
        ->where('transactionline.transactionID','=',$transactionID)
        ->where('transactionline.debit','=',$credit)
        ->get();
        
        foreach($recieveSheet as $value){
                $unroll = $value;

        }
        
        
    
        return $unroll;
       
    }
}
