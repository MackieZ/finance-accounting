<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class paymentInvoice extends Controller
{

    public function home(){
        return view('paymentInvoice');
    }

    public function getPaymentInvoice(Request $request){
        $invoicedate = $request->invoicedate;
        $clientid = $request->clientid;
        $paymenttype = $request->paymenttype;
        $productid = $request->productid;
        $amount = $request->amount;

        $invoiceid = DB::table('invoice')->insertGetId([
            'invoicedate' => $request->invoicedate,
            'clientid' => $request->clientid,
            'paymenttype' => $request->paymenttype
            ]);
        
        for($i=0;$i<count($productid);$i++){
            $data = array('invoiceid'=>$invoiceid,'invoicelinenumber'=>$i+1,
            'productid'=>$productid[$i],'amount'=>$amount[$i]);
            DB::table('invoiceline')->insert($data);
        }
        
        return redirect('/');
    }


}
