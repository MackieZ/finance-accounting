<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class departmentcontroller extends Controller
{
    public function form ()
    {
        return view('department');
    }

    public function cancel ()
    {
        return back();
    }

    public function submit (Request $request)
    
    { 
        $depid = $request->depid;
        $depcode = $request->depcode;
        $comid = $request->comid;
        $depname = $request->depname;
        DB::table('department')->insert([
            'DepartmentID' => $depid,
            'CompanyID' => $comid,
            'DepartmentCode' => $depcode,
            'DepartmentName' => $depname,
            ]);

        return back();
        
    }
}
