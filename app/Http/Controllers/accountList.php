<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class accountList extends Controller
{
    //
    public function home(){
        $roll = DB::table('account')
        ->join('transactionline','transactionline.accountID','=','account.accountID')
        ->select(DB::raw('account.AccountID, account.AccountName,account.accountType, sum(transactionline.Debit-transactionline.credit) as amount'))
        ->groupBy('account.accountID','account.AccountName','account.accountType')
        ->get();
    
        foreach($roll as $unroll){
            $account = $roll;
        }
        return view('accountList',compact('account'));
    }
}
