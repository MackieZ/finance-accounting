<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class StaffComplexController extends Controller
{
    public function form ()
    
    {   $departmentname=DB::table('department')->get(); 
        $companyname=DB::table('company')->get(); 
        $accountname=DB::table('account')->get();
        return view('staffcomplex',compact('companyname','departmentname','accountname'));
    }
    public function cancel ()
    {
        return back();
    }

    public function submit (Request $request)
    
    { 
        $stfirstname = $request->stfirstname;
        $stlastname = $request->stlastname;
        $staddress = $request->staddress;
        $stphone = $request->stphone;
        $did = $request->did;
        $aid = $request->aid;
        $cid = $request->cid;
        DB::table('staff')->insert([
            'StaffFirstName' => $stfirstname,
            'StaffLastName' => $stlastname,
            'StaffPhoneNumber' => $stphone,
            'StaffAddress' => $staddress,
            'DepartmentID' => $did,
            'AccountID' => $aid,
            'CompanyID' => $cid
            ]); 
        //DB::table('company')->insertGetId([])
        return back();
    }
}
