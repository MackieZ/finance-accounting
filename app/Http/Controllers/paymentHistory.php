<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class paymentHistory extends Controller
{
    //

    public function home(){
        return view('paymentHistory');
    }

    public function getPaymentHistory(Request $request){
        $clientid = $request->clientid;
        
        $query = self::getQuery($clientid);
        
        return view('viewPaymentHistory',compact('query','clientid'));
                
    }

    function getQuery($clientid){
        $query['query'] = DB::table('invoice')
        ->join('invoiceline','invoice.InvoiceID','=','invoiceline.InvoiceID')
        ->join('product','invoiceline.ProductID','=','product.ProductID')
        ->select(DB::raw('invoice.InvoiceDate as date,
        invoiceline.ProductID as product, invoiceline.Amount as amount,
        product.ProductPrice * invoiceline.Amount as total,
        invoice.InvoiceID as invoiceid'))       
        ->orderBy('invoice.InvoiceDate','asc')
        ->get();
        foreach($query as $value){
            $unroll = $value;
        }

        
        return $unroll;
    }

}
