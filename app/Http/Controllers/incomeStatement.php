<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class incomeStatement extends Controller
{
    //

    public function home(){
        return view('incomeStatement');
    }

    public function getIncomeStatement(Request $request){
        $start = $request->start;
        $end = $request->end;
        
        $revenue = self::getQuery('Revenue',$start,$end);
        $expense = self::getQuery('Expense',$start,$end);
        $totalRevenue = self::getTotal('Revenue',$start,$end);
        $totalExpense = self::getTotal('Expense',$start,$end);
        $net = $totalRevenue-$totalExpense;
        
        
        return view('viewIncomeStatement',compact('start','end','revenue','expense','totalRevenue','totalExpense','net'));
                
    }

    function getQuery($accountType,$start,$end){
        $query['query'] = DB::table('account')
        ->join('transactionline','account.AccountID','=','transactionline.AccountID')
        ->join('transaction','transactionline.TransactionID','=','transaction.TransactionID')
        ->join('invoiceTransaction','invoicetransaction.TransactionID','=','transaction.TransactionID')
        ->join('invoice','invoice.invoiceID','=','invoiceTransaction.invoiceID')
      #  ->join('company','company.CompanyID','=','transaction.CompanyID')
        ->select(DB::raw('account.accountName as name,account.accountID,SUM(transactionline.Debit-transactionline.Credit) as debit'))
        ->where('invoice.invoiceDate','>',$start)
        ->where('invoice.invoiceDate','<',$end)      
        ->where('account.accountType','=',$accountType)
        ->groupBy('account.accountName','account.accountID')
        ->get();
        return $query;
    }

    function getTotal($accountType,$start,$end){
        $total['total'] = DB::table('account')
        ->join('transactionline','account.AccountID','=','transactionline.AccountID')
        ->join('transaction','transactionline.TransactionID','=','transaction.TransactionID')
        ->join('invoiceTransaction','invoicetransaction.TransactionID','=','transaction.TransactionID')
        ->join('invoice','invoice.invoiceID','=','invoiceTransaction.invoiceID')
        ->join('company','company.CompanyID','=','transaction.CompanyID')        
        ->select(DB::raw('SUM(transactionline.Debit+transactionline.Credit) as debit'))
        ->where('invoice.invoiceDate','>',$start)
        ->where('invoice.invoiceDate','<',$end) 
        ->Where('account.accountType','like',$accountType)
        #->where('company.CompanyID','=','0')                
        ->groupBy('account.accountName')        
        ->get();
        $te=0;
        foreach($total as $value){
            foreach($value as $value2)
                $te=$value2->debit;
        }
        
            return $te;
    }
}
