<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class productSale extends Controller
{
    //
    public function home(){
        return view('productSale');
    }
    public function getProductSale(Request $request){
        $start = $request->start;
        $end = $request->end;
        $sale = self::query($start,$end);
        
        return view('viewProductSale',compact('start','end','sale'));
    }
    public function query($start,$end){
        $query = DB::table('invoiceline')
        ->join('product','invoiceline.productID','=','product.productID')
        ->join('invoice','invoice.invoiceID','=','invoiceline.invoiceID')
        ->where('invoice.invoiceDate','>',$start)
        ->where('invoice.invoiceDate','<',$end)
        ->select(DB::raw('Product.ProductID, Product.ProductName,product.productprice,
         sum(invoiceline.Amount) As Amount,
        sum(invoiceline.Amount)*product.productPrice as total'))
        ->groupBy('product.productID','product.productName','product.productprice')
        ->get();
        $i=0;
        foreach($query as $value){
            $unroll[$i] = $value;
            $i++;
        }
        return $unroll;
    }
}
