<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class trialBalance extends Controller
{
    //
    public function home(){
        return view('trialBalance');
    }

    public function  getTrialBalance(Request $request){
        $start = $request->start;
        $end = $request->end;
        $trial= self::query($start,$end);
        $sum=self::getsum($start,$end);
        return view('viewTrialBalance',compact('start','end','trial','sum'));
    }

    public function query($start,$end){
        $trial['trial'] = DB::table('account')
        ->join('transactionLine','transactionLine.accountID','=','account.accountID')
        ->join('transaction','transaction.transactionID','=','transactionline.transactionID')
        ->join('invoicetransaction','transaction.transactionID','=','invoicetransaction.transactionID')
        ->join('invoice','invoice.invoiceID','=','invoicetransaction.invoiceID')
        ->where('invoice.invoiceDate','>',$start)
        ->where('invoice.invoiceDate','<',$end)
        ->select(DB::raw('account.accountID,account.accountName,account.accountType,
        SUM(transactionline.Debit) as Debit,Sum(transactionLine.Credit) as Credit'))
        ->groupBy('account.accountID','account.accountName','account.accountType')
        ->orderBy('account.accountID')
        ->get();
        foreach($trial as $value){
            $unroll = $value;
        }

        return $unroll;
    }
    public function getsum($start,$end){
        $sum =  DB::table('account')
        ->join('transactionLine','transactionLine.accountID','=','account.accountID')
        ->join('transaction','transaction.transactionID','=','transactionline.transactionID')
        ->join('invoicetransaction','transaction.transactionID','=','invoicetransaction.transactionID')
        ->join('invoice','invoice.invoiceID','=','invoicetransaction.invoiceID')
        ->where('invoice.invoiceDate','>',$start)
        ->where('invoice.invoiceDate','<',$end)
        ->select(DB::raw('SUM(transactionline.Debit) as Debit,Sum(transactionLine.Credit) as Credit'))
        ->get();
        foreach($sum as $value){
            $unroll = $value;
        }
        return $unroll;
    }
}
