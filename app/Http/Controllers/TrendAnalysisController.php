<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TransactionLine;
use App\Account;
use DB;
use Carbon\Carbon;

class TrendAnalysisController extends Controller
{
    public function form ()
    {
        $Accounts = Account::get();
        foreach($Accounts as $Account){
            $Account['value'] = DB::table('account')
                -> join('transactionline','transactionline.accountID','=','account.accountID')
                -> join('transaction','transaction.transactionID','=','transactionline.transactionID')
                -> join('invoicetransaction','transaction.transactionID','=','invoicetransaction.transactionID')
                -> join('invoice','invoice.invoiceID','=','invoicetransaction.invoiceID')
                -> where('account.accountID', $Account -> AccountID)
                -> orderby('InvoiceDate')
                -> get()
                -> groupby( function ($date) {
                    return Carbon::parse($date->InvoiceDate) -> format('Y');
                });
                
            foreach($Account['value'] as $year => $value){
                $Total = 0;
                foreach ($value as $transaction){
                    $Total += $transaction->Debit - $transaction->Credit;
                }
                $Account['value'][$year] = $Total;
            }
        }        

        return view('trendAnalysis',['Accounts' => $Accounts]);
    }

    public function data ()
    {
            $yearTrend = DB::table('account')
                -> join('transactionline','transactionline.accountID','=','account.accountID')
                -> join('transaction','transaction.transactionID','=','transactionline.transactionID')
                -> join('invoicetransaction','transaction.transactionID','=','invoicetransaction.transactionID')
                -> join('invoice','invoice.invoiceID','=','invoicetransaction.invoiceID')
                -> orderby('InvoiceDate')
                -> get()
                -> groupby( function ($date) {
                    return Carbon::parse($date->InvoiceDate) -> format('Y');
                });
                
            foreach($yearTrend as $year => $value){
                $Total = 0;
                foreach ($value as $transaction){
                    $Total += $transaction->Debit - $transaction->Credit;
                }
                $yearTrend[$year] = $Total;
            }

        return response()
                -> json(['yearTrend' => $yearTrend]);

    }
}
