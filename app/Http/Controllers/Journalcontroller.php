<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;


class Journalcontroller extends Controller
{
       
    public function form ()
    {
        return view('journal');
    }

    public function cancel ()
    {
        return back();
    }

    public function submit (Request $request)
    
    { 
        $journame = $request->journame;
        $jourdes = $request->jourdes;
        DB::table('journal')->insert([
            'JournalName' => $journame,
            'JournalDescription' => $jourdes

            ]);

        return back();
        
    }
       
}
    

