<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class journalEntry extends Controller
{
    //
    public function home(){
        
        $rollcompany = DB::table('company')
        ->get();
        $i=0;
        foreach($rollcompany as $value){
            $company[$i] = $value;
            $i++;
        }

        $rollAccount = DB::table('account')
        ->get();
        $i=0;
        foreach($rollAccount as $value){
            $account[$i] = $value;
            $i++;
        }
        
        return view('journalEntry',compact('company','account'));
    }

    public function insertJournal(Request $request){
        $journal = $request->Name;
        $description = $request->Description;
        $company = $request->company;
        $date = $request->date;
        $account = $request->account;
        $debit = $request->debit;
        $credit = $request->credit;
       

        $data = array('JournalName'=>$journal,'JournalDescription'=>$description);
        $JournalID = DB::table('journal')->insertGetID($data);

        $data = array('JournalID'=>$JournalID,'CompanyID'=>$company);
        $transactionID = DB::table('transaction')->insertGetID($data);

        $data = array('PaymentType'=>'transaction','InvoiceDate'=>$date);
        $invoiceID = DB::table('invoice')->insertGetID($data);

        $data = array('InvoiceID'=>$invoiceID,'TransactionID'=>$transactionID);
        DB::table('invoicetransaction')->insert($data);

        $data = array('JournalID'=>$JournalID,'CompanyID'=>$company);
        DB::table('journalcompany')->insert($data);

      

        for($i=0;$i<count($account);$i++){
            if($debit[$i]==NULL) $debit[$i]=0;
            if($credit[$i]==NULL) $credit[$i]=0;
            $data=array('TransactionID'=>$transactionID,'TransactionLineNumber'=>$i,
            'Debit'=>$debit[$i],'Credit'=>$credit[$i],'AccountID'=>$account[$i]);
            DB::table('transactionline')->insert($data);
        }

        


        return view('/main');

    }


    function query($journalID){
        $query = DB::table('journal')
        ->join('journalcompany','journal.JournalID','=','journalcompany.JournalID')
        ->join('company','company.companyID','=','journalcompany.companyID')
        ->join('transaction','transaction.journalID','=','journal.journalID')
        ->join('transactionline','transactionline.transactionID','=','transaction.transactionID')
        ->join('account','account.accountID','=','transactionline.accountID')
        ->join('invoiceTransaction','invoicetransaction.transactionID','=','transaction.transactionID')
        ->join('invoice','invoice.invoiceID','=','invoiceTransaction.invoiceID')
        ->where('journal.journalID','=',$journalID)
        ->get();
        $i=0;
        foreach($query as $value){
            $unroll[$i]=$value;
            $i++;
        }
        return $query;
    }
}
