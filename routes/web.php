<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main');
});

Route::get('/testview','testview@getData');


Route::get('/balanceSheet','balanceSheet@getBalanceSheet');

Route::get('/incomeStatement','incomeStatement@home');
Route::post('/incomeStatement/new','incomeStatement@getIncomeStatement');

Route::get('/generalJournal','generalJournal@home');
Route::post('/generalJournal/report','generalJournal@getGeneralJournal');

Route::get('/generalLedger','generalLedger@home');
Route::post('/generalLedger/report','generalLedger@getGeneralLedger');

Route::get('/trialBalance','trialBalance@home');
Route::post('/trialBalance/report','trialBalance@getTrialBalance');

Route::get('/paymentHistory','paymentHistory@home');
Route::post('/paymentHistory/report','paymentHistory@getPaymentHistory');

Route::get('/addTransaction','addTransaction@home');
Route::post('/addTransaction/report','addTransaction@getAddTransaction');
Route::post('/editTransaction/report','addTransaction@getEditTransaction');
Route::post('/deleteTransaction/report','addTransaction@getDeleteTransaction');

Route::get('/paymentInvoice','paymentInvoice@home');
Route::post('/paymentInvoice/report','paymentInvoice@getPaymentInvoice');
Route::get('/recieveSheet','recieveSheet@home');
Route::post('/recieveSheet/report','recieveSheet@getRecieveSheet');

Route::get('/accountList','accountList@home');

Route::get('/commonSizeBalanceSheet','commonSizeBalanceSheet@home');

Route::get('/invoiceSheet','invoicesheet@home');
Route::post('/invoiceSheet/report','invoiceSheet@getinvoiceSheet');

Route::get('/productSale','productSale@home');
Route::post('/productSale/report','productSale@getProductSale');


Route::get('/journalEntry','journalEntry@home');
Route::post('/journalEntry/add','journalEntry@insertJournal');


Route::get('/editJournal','editJournal@home');
Route::post('editJournal/submit','editJournal@getJournal');
Route::post('/journalEdit','editJournal@journalEdit');
Route::post('/companyEdit','editJournal@companyEdit');
Route::post('/invoiceEdit','editJournal@invoiceEdit');
Route::post('/transactionlineEdit','editJournal@transactionlineEdit');


Route::get('/test', function () {
   # return view('test');
    return View::make('test');
});


Route::get('/simple-form1', function () {
    return view('simple-form1');
});

Route::get('/check-connect',function(){
    if(DB::connection()->getDatabaseName())
    {
    return "Yes! successfully connected to the DB: " . DB::connection()->getDatabaseName();
    }else{
    return 'Connection False !!';
    }
   });


   Route::get('check-model','Contrillserstaff@getIndex');
// Route::get('/simple-form1', function () {
//     return view('simple-form1');
// });

Route::get('/simple-form1', 'SimpleForm1Controller@home');
Route::post('/incomeStatement/new', 'incomeStatement@getIncomeStatement');
Auth::routes();

Route::get('/product', 'ProductController@form')->name('product');
Route::post('/product/new', 'ProductController@submit')->name('product');
Route::get('/staff', 'ControllStaff@form')->name('Staff');
Route::post('/staff/new', 'ControllStaff@submit')->name('Staff');
Route::get('/company', 'CompanyController@form')->name('company');
Route::post('/company/new', 'CompanyController@submit')->name('company');
Route::get('/account', 'Accountcontroller@form')->name('account');
Route::post('/account/new', 'Accountcontroller@submit')->name('account');

Route::get('/client', 'Clientcontroller@form')->name('client');
Route::post('/client/new', 'Clientcontroller@submit')->name('client');
Route::get('/journal', 'Journalcontroller@form')->name('journal');
Route::post('/journal/new', 'Journalcontroller@submit')->name('journal');

Route::get('/StaffComplex', 'StaffComplexController@form');
Route::post('/StaffComplex/new', 'StaffComplexController@submit');

Route::get('/editstaffcom', 'editstaffController@form');
Route::post('/editstaff/report', 'editstaffController@report');
Route::post('/editstaff/putdata', 'editstaffController@putdata');

Route::get('/department', 'departmentcontroller@form')->name('department');
Route::post('/department/new', 'departmentcontroller@submit')->name('department');

Route::get('/test', 'tests@form')->name('test');
Route::post('/test/new', 'tests@submit')->name('test');
Route::get('/trendanalysis', 'TrendAnalysisController@form')->name('trendanalysis');
Route::POST('/trendanalysis', 'TrendAnalysisController@data')->name('trendanalysis');

Route::get('/account-complex', 'AccountComplexController@form')->name('account');
Route::get('/account-complex/{id}/edit', 'AccountComplexController@edit')->name('account');
Route::post('/account-complex/editSave', 'AccountComplexController@editSave')->name('account.editSave');

Route::get('/Home2', 'balanceSheet@getTotal');
Route::get('/Home3', 'balanceSheet@getTotalAccount');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
