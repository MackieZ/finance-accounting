@extends('headbar')

@section('content')
<div class="content-form">
    <div class="container">
        <form method = "post" action="/addTransaction/report">
        {{csrf_field()}}
            <h3>Add Transaction</h3>            
            JournalID: 
            <br>
            <input type="text" name="journalid">
            <br>
            CompanyID: 
            <br>
            <input type="text" name="companyid">
            <br> 
            <br>
            <font color="red">*Only fill the following if the transaction is an invoice</font> 
            <br>
            InvoiceID:
            <br>
            <input type="text" name="invoiceid">
            <br>
            InvoiceDate: 
            <br>
            <input type="date" value="<?php print(date("Y-m-d")); ?>"/ name="invoicedate">
            <br>
            ClientID: 
            <br>
            <input type="text" name="clientid">
            <br>
            PaymentType: 
            <br>
            <input type="text" name="paymenttype">
            <br>
            <br><button class="btn">Add Transaction</button>  
        </form>
    </div>
    <div class="container">
        <form method = "post" action="/editTransaction/report">
        {{csrf_field()}}
            <h3>Edit Transaction</h3> 
            TransactionID: 
            <br>
            <input type="text" name="transactionid">
            <br>
            JournalID: 
            <br>
            <input type="text" name="journalid">
            <br>
            CompanyID: 
            <br>
            <input type="text" name="companyid">
            <br> 
            <br><button class="btn">Edit Transaction</button>  
        </form>
        <form method = "post" action="/deleteTransaction/report">
        {{csrf_field()}}
            <br>
            <h3>Delete Transaction</h3> 
            TransactionID: 
            <br>
            <input type="text" name="transactionid">
            <br> 
            <br><button class="btn">Delete Transaction</button>  
        </form>
    </div>    
</div>




@endsection