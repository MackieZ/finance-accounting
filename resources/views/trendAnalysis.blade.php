@extends('headbar')

@section('header')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.4.3/css/mdb.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.4.3/js/mdb.min.js"></script>
@endsection

@section('content-header')
    <!-- <form method = "post" action="/product/new">  -->  <!-- POST FORM แบบ 1 (PHP) -->
    <div class="container-fluid">
        <div class="content-form">
                <div class="col-md-6 col-left" >
                    <div class="row title">
                        <span class="glyphicon glyphicon-signal" style="margin-right: 1rem"></span>Trend Analysis
                        <hr>
                    </div>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#Cash</th>
                                @if(count($Accounts))
                                    @foreach ($Accounts as $Account)
                                        @if($loop -> first)
                                            @foreach ($Account -> value as $key => $value)
                                                <th scope="col">{{$key }}</th>
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($Accounts))
                                @foreach ($Accounts as $Account)
                                <tr onclick='trClick()' value="" id="" class="trAccount">
                                    <th scope="row">{{ $Account -> AccountName }}</th>
                                    @foreach ($Account -> value as $key => $value)
                                        <td>{{ $value }}</td> 
                                    @endforeach
                                </tr>    
                                @endforeach
                            @else
                                <tr> <th scope="row">No Record</th> </tr>
                            @endif
                        </tbody>
                    </table>
                
                </div>
                <div class="col-md-6 col-right vr-line">
                    <canvas id="barChart"></canvas>
                </div>
        </div>
    </div>
    <!-- </form> POST FORM แบบ 1 (PHP) -->
    <div>
@endsection

@section('style')
    <style>
        .title {
            font-size: 28px; 
            margin: 20px 0px;  
            color: #3097D1;
        }
        hr {
            border: 1px solid #3097D1;
            margin: 15px 0px 10px 0px;
        }
        iframe {
             margin-top: auto !important;
            margin-bottom: auto !important;
             
        }
        .vr-line {
            border-left: 1px solid #36393f;
        }

        .col-left {
            padding: 15px 35px 15px 0px;
        }

        .col-right {
            padding: 15px 0px 15px 35px;
        }

        .content-form {
            margin: 35px auto;
            padding: 15px 40px 40px 40px;
            background-color: #efefef;
            color: #3f3f3f;
            height: auto;
            width: 97%;
            min-height: 500px;
            /* display: flex; */
            /* justify-content: space-between; */
            border-radius: 15px;
            border: 2px solid #36393f;
            /* box-shadow: 0px 0px 0px 3px #efefef; */
        }
    </style>
@endsection

@section('script')
    <script>
        $(document).ready(function (){
            // POST FORM แบบ 2 AJAX
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.cancel-btn').click(function(){tren
                window.location.href = '/';
            });
           
            $.ajax({
                url: '/trendanalysis',
                type: 'POST',
                cache: false,
                encoding: "UTF-8",
                data: {
                },
                success: function(response){
                    console.log("JSON :", response);
                    
                    var obj = response.yearTrend;
                    var labelsTrend = []
                    var labelsTrendValue = []

                    for(var i in obj){
                        
                        labelsTrend.push(i);
                        labelsTrendValue.push(obj[i]);
                    }
                    // labelsTrend.push(0);
                    labelsTrendValue.push(0);
                    var ctxB = document.getElementById("barChart").getContext('2d');
                    var myBarChart = new Chart(ctxB, {
                        type: 'bar',
                        data: {
                            labels: labelsTrend,
                            datasets: [{
                                label: '# of DebitCredits',
                                data: labelsTrendValue,
                                backgroundColor: [
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(54, 162, 235, 0.2)',
                                    'rgba(255, 206, 86, 0.2)',
                                    'rgba(75, 192, 192, 0.2)',
                                    'rgba(153, 102, 255, 0.2)',
                                    'rgba(255, 159, 64, 0.2)'
                                ],
                                borderColor: [
                                    'rgba(255,99,132,1)',
                                    'rgba(54, 162, 235, 1)',
                                    'rgba(255, 206, 86, 1)',
                                    'rgba(75, 192, 192, 1)',
                                    'rgba(153, 102, 255, 1)',
                                    'rgba(255, 159, 64, 1)'
                                ],
                                borderWidth: 2
                            }]
                        },
                        optionss: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }]
                            }
                        }
                    });
                    // window.location.href = '/';
                },
                error: function(error){
                    console.log("ERROR :", error);
                }
            });
            
            
            


        });

    </script>
@endsection
