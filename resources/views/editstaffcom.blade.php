@extends('headbar')

@section('content')
    <form method = "post" action="/editstaff/report">
    {{csrf_field()}}
        <div class="content-form">
            <div class="container">
                <div class="row">
                    <h3>Edit Staff</h3>
                </div>
                <div class="row">
                <span>Staff ID: </span>
                     <select name="sid">
                     <option value="0" disabled="true" selected="true">Staff ID</option>
                        @foreach($allstaff as $astaff)
                            <option value="{{$astaff->StaffID}}">{{$astaff->StaffID}}</option>
                         @endforeach
                     </select>
                <br>
                <button class="btn">Click</button>
                <br>
                
            </div>        
        </div>
    </form>
@endsection

