@extends('headbar')

@section('content')
    <div class="content-form">
    <div class="container">
    <div class="row">
        <h3>Invoice Sheet</h3>
    </div>
    <div class="row">
        <p>Date: {{$date}}</p>  
        <div class="col" style="font-size:12px;">Client Name:{{$clientName}}</div> 
    </div>


    <div class="row">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">ProductName</th>
                    <th scope="col">Product<br>Description</th>
                    <th scope="col">Price</th>
                    <th scope="col">Amount</th>
                    <th scope="col">Amount<br>Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach($invoice as $data)
                <tr>
                <td>{{$data->ProductName}}</td>
                <td>{{$data->Descriptioin}}</td>
                <td>{{$data->Price}}</td>
                <td>{{$data->Amount}}</td>
                <td>{{$data->Total}}</td>
                </tr>
            @endforeach
            <tr>
                <td></td>
                <td></td>
                <td>Totals</td>
                <td></td>
                <td>{{$total}}</td>
                </tr>
            </tbody>

        </table>
    </div>

    </div>
    </div>
    </div>
    
@endsection

