@extends('headbar')

@section('content')
<form method = "get" action="/">
    <div class="content-form">
    <div class="container">
    
    @if ($mode == 'add') Data inserted. 
    @elseif ($mode == 'edit') Data edited.
    @elseif  ($mode == 'delete') Data deleted.
    @endif
    <br>
    Click the button to go back to the home page.<br>
    <br>
    <button class="btn">Home Page</button>  
    </div>
    </div>
</form>   
    
@endsection