@extends('headbar')

@section('content')
    <div class="content-form">
        <div class="container">
            <div class="row">
                <h3>edit Journal</h3>
            </div>

            <div class="row">
                <form action="/journalEdit" method="post">
                {{ csrf_field() }}
                    <table>
                        <tr>
                            <th>JournalName</th>
                            <th><input type="text" name="newJournal" Value={{$journal[0]->JournalName}} ></th>
                        </tr>
                        <tr>
                            <th>Description</th>
                            <th><input type="text" name="newDescription" value={{$journal[0]->JournalDescription}}></th>
                        </tr>
                    </table>
                    <input type="hidden" value= {{$journal[0]->JournalID}} name = "journalID">
                    <button class="btn">Edit</button>
                </form>
            </div>
    
            

            <div class="row">
                <form action="/companyEdit" method="post">
                    {{ csrf_field() }}
                    <label for="company">Company</label>
                    <select name="companyID" class="form-control">
                        @foreach($company as $data)
                          <option value="{{$data->CompanyID}}">{{$data->CompanyName}}</option>
                        @endforeach
                    </select>
                    <input type="hidden" value= {{$transaction->TransactionID}} name="transactionID">
                    <button class="btn">Edit</button>
                </form>
            </div>



            <div class="row">
                <form action="/invoiceEdit" method="post">
                         {{ csrf_field() }}
                        <label for="date">Date</label>
                        <input type="date" class="form-control" name="date" Value = {{$transaction->InvoiceDate}} >
                        <input type="hidden" name="invoiceID" Value = {{$transaction->InvoiceID}}>
                    <button class="btn">Edit</button>
                </form>
            </div>

            <label for="transaction">Transaction</label>



            <table>
            <tr>
            <th>Account</th>
            <th>Debit</th>
            <th>Credit</th>
            </tr>
            </table>
            @foreach($transactionline as $value)
            <div class="row">
                <form action="/transactionlineEdit" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" value= {{$transaction->TransactionID}} name="transactionID">
                    <input type="hidden" value= {{$value->TransactionLineNumber}} name="transactionlineID">

                    <table>
                        <td><select name="account" class="form-control" value={{$value->AccountID}}>
                        @foreach($account as $data)
                             <option value="{{$data->AccountID}}">{{$data->AccountName}}</option>
                          @endforeach
                        </select></td> 
                        <td><input type="number" name="debit" style="width: 100%;" value = {{$value->Debit}} ></td>
                         <td><input type="number" name="credit" style="width: 100%;" value = {{$value->Credit}} ></td>
                        <td><button class="btn">Edit</button></td>
                    </table>
                </form>
            </div>
            @endforeach


            </div>
        </div>
    </div>
    
@endsection

