@extends('headbar')

@section('content')
    <form method = "post" action="/generalLedger/report">
    {{csrf_field()}}
        <div class="content-form">
            <div class="container">
                <div class="row">
                    <h3>General Ledger</h3>
                </div>
                <div class="row">
                    <div class="form-group">
                    <label for="cpn">period:</label>
                    <input type="date" name="start">
                    to
                    <input type="date" name="end">
                    <div class="row">
                        <select name="no" id="no">
                        @foreach($account as $data)
                            <option value={{$data->AccountID}} >No.{{$data->AccountID}}:{{$data->AccountName}}</option>
                        @endforeach
                        </select>
                        
                   </div>
                </div>
                <button class="btn">Click</button>
                <br>
                
            </div>        
        </div>
    </form>
@endsection
