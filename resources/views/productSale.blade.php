@extends('headbar')

@section('content')
    <form method = "post" action="/productSale/report">
    {{csrf_field()}}
        <div class="content-form">
            <div class="container">
                <div class="row">
                    <h3>Product Sale</h3>
                </div>
                <div class="row">
                    <div class="form-group row">
                    <label for="cpn">period:</label>
                    <div class="col">
                    <input type="date" name="start">
                    to
                    <input type="date" name="end">
                    </div>
                </div>
                <button class="btn">Click</button>
                <br>
                
            </div>        
        </div>
    </form>
@endsection
