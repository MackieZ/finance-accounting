@extends('headbar')

@section('content')
      <form method = "post" action="/paymentInvoice/report">
      {{csrf_field()}}
      <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script> -->
      <div class="content-form">
            <div class="container">    
            <h2>Payment Invoice</h2>
                  <br>
                  Invoice Date:
                  <br>
                  <input type="date" value="<?php print(date("Y-m-d")); ?>"/ name="invoicedate">
                  <br>
                  ClientID:
                  <br>
                  <input type="text" name="clientid">
                  <br>
                  Payment Type:
                  <br>
                  <select name="paymenttype">
                        <option value="Visa">Visa</option>
                        <option value="Mastercard">Mastercard</option>
                        <option value="Paypal">Paypal</option>
                  </select>
                  <br><br>
                  Invoice Line
                  <br>
                  <div class="form-group">  
                        <form name="add_name" id="add_name">  
                              <div class="table-responsive">  
                                    <table class="table table-bordered" id="dynamic_field">  
                                    <tr>  
                                          <td><input type="text" name="productid[]" placeholder="Insert ProductID" class="form-control name_list" /></td>  
                                          <td><input type="number" name="amount[]" placeholder="Amount" class="form-control name_list"></td>
                                          <td><button type="button" name="add" id="add" class="btn btn-success">Add</button></td>  
                                    </tr>  
                                    </table>  
                                    <input type="submit" name="submit" id="submit" class="btn btn-info" value="Submit" />  
                              </div>  
                        </form>  
                  </div>  
            </div>      
      </div>
      </form>
      <script>  
      $(document).ready(function(){  
            var i=1;  
            $('#add').click(function(){  
                  i++;  
                  $('#dynamic_field').append('<tr id="row'+i+'"><td><input type="text" name="productid[]" placeholder="Insert ProductID" class="form-control name_list" /></td><td><input type="number" name="amount[]" placeholder="Amount" class="form-control name_list"></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">Remove</button></td></tr>');  
            });  
            $(document).on('click', '.btn_remove', function(){  
                  var button_id = $(this).attr("id");   
                  $('#row'+button_id+'').remove();  
            });  
      });  
      </script>
@endsection




 