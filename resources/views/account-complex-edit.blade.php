@extends('headbar')

@section('content')
<form method = "post" action="/account-complex/editSave">
    {{csrf_field()}}
    <div class="content-form">
        <div class="container">
            <div class="row title">
                <span class="glyphicon glyphicon-pencil" style="margin-right: 1rem"></span> Edit Account ID : {{ $data -> AccountID}} 
                <input type="hidden" value="{{ $data -> AccountID}}" name="accId" id="accId">
                <hr>
            </div>
            <div class="form-group">
                <label for="accountname">Account Name</label>
                <input type="text" class="form-control" name="accountname" id="accountname" placeholder="Account Name" value = "{{$data -> AccountName}}">
            </div>
            <div class="form-group">
                <label for="accounttype">Account Type</label>
                <select class="form-control" name="accounttype">
                <option value="0" disabled="true" selected="true">Account Type</option>
                        <option value="Assets">Assets</option>
                        <option value="Liability">Liability</option>
                        <option value="Equity">Equity</option>
                        <option value="Revenue">Revenue</option>
                        <option value="Expense">Expense</option>
                  </select>
            </div>
            <div class="form-group">
                <label for="sel1">Staff Account : </label> <span id="staffText"> {{ $data -> StaffFirstName }} {{ $data -> StaffLastName }} </span>
                <select class="form-control" id="sel1" name="sel1" value="{{ $data -> StaffFirstName }}">
                    @foreach($staff as $staff)
                        @if($staff -> StaffID == $data -> StaffID) 
                        <option selected value="{{ $staff -> StaffFirstName }}">
                            {{$staff -> StaffFirstName}} {{$staff->StaffLastName}}
                        </option>
                        @else
                        <option value="{{ $staff -> StaffFirstName }}">
                            {{$staff -> StaffFirstName}} {{$staff->StaffLastName}}
                        </option>
                        @endif
                    @endforeach
                </select>
            </div>
            @foreach($accountcompany as $accounts)
                <div class="form-group">
                    <label for="sel2">Company Account : </label> <span id="companyText"> {{ $accounts -> CompanyName }}  </span>
                    <select class="form-control sel2" name="sel2[]" id="sel2" value="{{ $accounts -> CompanyName }} ">
                        @foreach($company as $comp)
                            @if($comp -> CompanyID == $accounts -> CompanyID) 
                            <option selected value="{{$comp -> CompanyName}}">
                                {{$comp -> CompanyName}}
                            </option>
                            @else
                            <option value="{{$comp -> CompanyName}}">
                                {{$comp -> CompanyName}}
                            </option>
                            @endif
                        @endforeach
                    </select>
                </div>
            @endforeach
            <div class="row margin-top20">
                <div class="col-12 col-md-12 margin-top20">
                    <button disabled type="submit" class="btn btn-primary submit-btn">Save</button>
                    <button type="button" class="btn btn-secondary cancel-btn">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('style')
    <style>
        tbody > tr:hover {
            opacity: .4;
            cursor: pointer;
        }

        .title {
            font-size: 28px; 
            margin: 20px 0px;  
            color: #3097D1;
        }
        hr {
            border: 1px solid #3097D1;
            margin: 15px 0px 10px 0px;
        }
    </style>
@endsection

@section('script')
    <script>

        $(document).ready(function (){
            // POST FORM แบบ 2 AJAX
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.cancel-btn').click(function(){
                window.location.href = '/';
            });

            $('#sel1').on('change', function(){
                $('#staffText').text(
                    $('#sel1').find(':selected').val()
                );
            });

            $('#sel2').on('change', function(){
                $('#companyText').text(
                    $('#sel2').find(':selected').val()
                );
            });

            //Validate on Load
            $('.submit-btn').attr("disabled", function(){
                    if( $('#accountname').val() === '' || 
                        $('#accounttype').val() === ''
                    )   return true;
                    return false;
            });

            //Validate Input
            $('#accountname, #accounttype').on('keyup', function(){
                $('.submit-btn').attr("disabled", function(){
                    if( $('#accountname').val() === '' || 
                        $('#accounttype').val() === ''
                    )   return true;
                    return false;
                });
            });

            // //POST FORM แบบ 2 AJAX

            // $('.submit-btn').click(function(){
            //     $.ajax({
            //         url: '/account-complex/' + accId +'/edit',
            //         type: 'POST',
            //         cache: false,
            //         encoding: "UTF-8",
            //         data: {
            //             'accId' : $('#accId').val(),
            //             'accountname' : $('#accountname').val(),
            //             'accounttype' : $('#accounttype').val(),
            //             'staff' : $('#sel1').find(':selected').val(),
            //             'company' : $('.sel2').val()
            //         },
            //         success: function(response){
            //             console.log("POST :", response);
                        
            //             // window.location.href = '/';
            //         },
            //         error: function(error){
            //             console.log("ERROR :", error);
            //         }
            //     });
            // });


        });

    </script>
@endsection
