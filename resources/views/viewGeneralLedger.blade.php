@extends('headbar')

@section('content')
    <div class="content-form">
    <div class="container">
    <div class="row">
        <h3>General Journal</h3>
    </div>
    <div class="row">
        <p>period {{$start}} to {{$end}}</p>  
        <div class="col" style="font-size:12px;">AccountNo:{{$account->AccountID}}</div> 
        <div class="col" style="font-size:12px;">AccountName:{{$account->AccountName}}</div>
    </div>


    <div class="row">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">Date</th>
                    <th scope="col">TransactionNo.</th>
                    <th scope="col">Description</th>
                    <th scope="col">Debit</th>
                    <th scope="col">Credit</th>
                    <th scope="col">Balance</th>
                </tr>
            </thead>
            <tbody>
                @foreach($ledger as $data)
                <tr>
                <td>{{$data->InvoiceDate}}</td>
                <td>{{$data->TransactionID}}</td>
                <td>{{$data->JournalDescription}}</td>
                <td>{{$data->Debit}}</td>
                <td>{{$data->Credit}}</td>
                <td>{{$data->balance}}</td>
                </tr>
            @endforeach
            <tr>
                <td></td>
                <td></td>
                <td>period Totals</td>
                <td>{{$td}}</td>
                <td>{{$tc}}</td>
                <td></td>
                </tr>
            </tbody>

        </table>
    </div>

    </div>
    </div>
    </div>
    
@endsection

