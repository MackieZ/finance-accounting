@extends('headbar')

@section('content')
    <!-- <form method = "post" action="/product/new">  -->  <!-- POST FORM แบบ 1 (PHP) -->
    <div class="content-form">
        <div class="container">
            <div class="row title">
                Accounts List
                <a href="/account"
                    <button type="button" class="btn btn-success" style="margin-left: 13rem"> <span class="glyphicon glyphicon-plus-sign" ></span> Add Account</button>
                </a>
                <hr>
            </div>
           
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">AccountName</th>
                        <th scope="col">AccountType</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($listAccount))
                        @foreach ($listAccount as $list)
                        <tr onclick='trClick({{$list -> AccountID}})' value="" id="{{$list -> AccountID}}" class="trAccount">
                            <th scope="row">{{ $loop -> iteration }}</th>
                            <td>{{ $list -> AccountName}}</td>
                            <td>{{ $list -> AccountType}}</td>         
                            <td><span class="glyphicon glyphicon-pencil" ></span></td>            
                        </tr>    
                        @endforeach
                    @else
                        <tr> <th scope="row">No Record</th> </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    <!-- </form> POST FORM แบบ 1 (PHP) -->
@endsection

@section('style')
    <style>
        tbody > tr:hover {
            opacity: .4;
            cursor: pointer;
        }

        .title {
            font-size: 28px; 
            margin: 20px 0px;  
            color: #3097D1;
        }
        hr {
            border: 1px solid #3097D1;
            margin: 15px 0px 10px 0px;
        }
    </style>
@endsection

@section('script')
    <script>
        function trClick (AccountID) {
            
            console.log(AccountID);
            location.href = 'account-complex/' + AccountID + '/edit/';
            // $.ajax({
            //     url: 'account-complex/edit/' + AccountID,
            //     type: 'GET',
            //     cache: false,
            //     encoding: 'UTF-8',
            //     data: {
            //         accountId: AccountID
            //     },
            //     success: function(response) {
            //         console.log("Success: ", response);
            //     },
            //     error: function(error) {
            //         console.log("Error: ", error);
            //     }
            // })
        }

        $(document).ready(function (){
            // POST FORM แบบ 2 AJAX
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.cancel-btn').click(function(){
                window.location.href = '/';
            });

            //POST FORM แบบ 2 AJAX
            // $('.trAccount').click(function(){
            //     console.log($(this).find(th).val());
            //     $.ajax({
            //         url: '/account/new',
            //         type: 'POST',
            //         cache: false,
            //         encoding: "UTF-8",
            //         data: {
            //             accountname: $('#accountname').val(),
            //             accounttype: $('#accounttype').val()
            //         },
            //         success: function(response){
            //             console.log("POST :", response);
                        
            //             window.location.href = '/';
            //         },
            //         error: function(error){
            //             console.log("ERROR :", error);
            //         }
            //     });
            // });

        });

    </script>
@endsection
