@extends('headbar')

@section('content')
    <div class="content-form">
    <div class="container">
    <div class="row">
        <h3>Payment History</h3>
    </div>
    <div class="row">
        <p>ClientID: {{$clientid}}</p>  
    </div>


    <div class="row">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">Date</th>
                    <th scope="col">Product</th>
                    <th scope="col">Amount</th>
                    <th scope="col">Total</th>
                    <th scope="col">InvoiceID</th>
                </tr>
            </thead>
            <tbody>
                @foreach($query as $data)
                <tr>
                <td>{{$data->date}}</td>
                <td>{{$data->product}}</td>
                <td>{{$data->amount}}</td>
                <td>{{$data->total}}</td>
                <td>{{$data->invoiceid}}</td>
                </tr>
                @endforeach
            </tbody>

        </table>
    </div>

    </div>
    </div>
    
    
@endsection

