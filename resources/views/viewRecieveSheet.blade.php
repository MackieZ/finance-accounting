@extends('headbar')

@section('content')
    <div class="content-form">
    <div class="container">
    <div class="row">
        <h3>Recieve Sheet</h3>
    </div>
    <div class="row">
        <p>period {{$start}} to {{$end}}</p>  
    </div>


    <div class="row">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">Date</th>
                    <th scope="col">Account<br>No.</th>
                    <th scope="col">Account<br>Name</th>
                    <th scope="col">Account<br>Recieve</th>
                    <th scope="col">Account<br>Recieve<br>Name</th>
                    <th scope="col">Amount</th>
                </tr>
            </thead>
            <tbody>
                @foreach($value3 as $data)
                <tr>
                <td>{{$data->pay->InvoiceDate}}</td>
                <td>{{$data->pay->AccountID}}</td>
                <td>{{$data->pay->AccountName}}</td>
                <td>{{$data->AccountID}}</td>
                <td>{{$data->AccountName}}</td>
                <td>{{$data->Debit}}</td>
                </tr>
            @endforeach
            
            </tbody>

        </table>
    </div>

    </div>
    </div>
    </div>
    
@endsection

