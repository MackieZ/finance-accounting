@extends('headbar')

@section('content')
    <form method = "post" action="/incomeStatement/new">
    {{csrf_field()}}
        <div class="content-form">
            <div class="container">
                <div class="row">
                    <h3>Income Statement</h3>
                </div>
                <div class="row">
                    <div class="form-group row">
                    <label for="cpn">Period : </label>
                    <div class="col">
                    <input type="date" name="start">
                    To
                    <input type="date" name="end">
                    </div>
                </div>
                <button class="btn">Click</button>
                <br>
                
            </div>        
        </div>
    </form>
@endsection
