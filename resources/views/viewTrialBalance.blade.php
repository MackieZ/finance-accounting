@extends('headbar')

@section('content')
    <div class="content-form">
    <div class="container">
    <div class="row">
        <h3>Trial Balance</h3>
    </div>
    <div class="row">
        <p>period {{$start}} to {{$end}}</p>  
    </div>


    <div class="row">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">AccountNo.</th>
                    <th scope="col">AccountName</th>
                    <th scope="col">Type</th>
                    <th scope="col">Debit</th>
                    <th scope="col">Credit</th>
                </tr>
            </thead>
            <tbody>
                @foreach($trial as $data)
                <tr>
                <td>{{$data->accountID}}</td>
                <td>{{$data->accountName}}</td>
                <td>{{$data->accountType}}</td>
                <td>{{$data->Debit}}</td>
                <td>{{$data->Credit}}</td>
                </tr>
            @endforeach
            <tr>
                <td></td>
                <td></td>
                <td>period Totals</td>
                <td>{{$sum->Debit}}</td>
                <td>{{$sum->Credit}}</td>
                <td></td>
                </tr>
            </tbody>

        </table>
    </div>

    </div>
    </div>
    </div>
    
@endsection

