@extends('headbar')

@section('content')
    <div class="content-form">
    <div class="container">
    <div class="row">
        <h3>Common Size Balance Sheet</h3>
    </div>
   
    <div class="row">
    <table class="table table-hover" >
    <thead>
        <th></th>
        <th>Amount</th>
        <th>Percentage</th>
  </thead>
  <thead >
    <tr>
      
      <th style="padding-right:50%; font-size:18px;">Asset</th>
      <th > </th>
     
    </tr>
  </thead>
  <tbody>
  @foreach($asset as $value)
    <tr >
      <td style="font-size:15px;">{{$value->name}}</td>
      <td style="font-size:15px;">{{$value->debit}}</td> 
      <td style="font-size:15px;">{{$value->p}} </td>
    </tr>
    @endforeach
   
    <tr >
      <td style="font-size:15px;">Total Asset</td>
      <td style="font-size:15px;">{{$totalAsset}}</td>
      <td style="font-size:15px;">100</td>

    </tr>
    
  </tbody>
  <thead>
    <tr> <td style="font-size:19px"> Liabilities & Equity </td> </tr>
  </thead>
    <tr>
      <th style="padding-left:10%; padding-right:50%; font-size:16px;">Liabilities</th>
      <th > </th>
    </tr>
  <tbody>
  @foreach($liability as $value)
    <tr >
      <td style="font-size:15px;">{{$value->name}}</td>
      <td style="font-size:15px;">{{$value->debit}}</td>  
      <td style="font-size:15px;">{{$value->p}}</td>
    </tr>
    @endforeach     
  </tbody>
  <tr>
      <th style="padding-left:10%; padding-right:50%; font-size:16px;">Equity</th>
      <th > </th>
    </tr>
  <tbody>
  @foreach($equity as $value)
    <tr >
      <td style="font-size:15px;">{{$value->name}}</td>
      <td style="font-size:15px;">{{$value->debit}}</td>  
      <td style="font-size:15px;">{{$value->p}}</td>
    </tr>
    @endforeach
  </tbody>
  <thead>
    
    <tr >
      <td style="font-size:18px;">Total Liability&Equity</td>
      <td style="font-size:15px;">{{$totalCredit}}</td> 
      <td style="font-size:15px;">100</td>
    </tr>
   
  </thead>
</table>

    
    </div>
    </div>
    </div>
    
@endsection

