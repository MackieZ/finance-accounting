@extends('headbar')

@section('content')
    <!-- <form method = "post" action="/product/new">  -->  <!-- POST FORM แบบ 1 (PHP) -->
    <div class="content-form">
        <div class="container">
            <div class="row title">
                <span class="glyphicon glyphicon-plus-sign" style="margin-right: 1rem"></span>Add Journal
                <hr>
            </div>
            <div class="form-group">
                <label for="journame">Journal Name</label>
                <input type="text" class="form-control" id="journame" placeholder="Journal Name">
                <!-- 
                    ถ้าจะ POST FORM แบบ 1 ใส่ name="productName" ด้วย 
                    <input type="text" class="form-control" name="productName" id="productName" placeholder="Name">
                -->
            </div>
            <div class="form-group">
                <label for="jourdes">Journal Description</label>
                <input type="text" class="form-control" id="jourdes" placeholder="Journal Description">
            </div>
            <div class="row margin-top20">
                <div class="col-12 col-md-12 margin-top20">
                    <button disabled type="button" class="btn btn-primary submit-btn">Submit</button>
                    <button type="button" class="btn btn-secondary cancel-btn">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!-- </form> POST FORM แบบ 1 (PHP) -->
@endsection

@section('style')
    <style>
        .title {
            font-size: 28px; 
            margin: 20px 0px;  
            color: #3097D1;
        }
        hr {
            border: 1px solid #3097D1;
            margin: 15px 0px 10px 0px;
        }
    </style>
@endsection

@section('script')
    <script>
        $(document).ready(function (){
            // POST FORM แบบ 2 AJAX
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.cancel-btn').click(function(){
                window.location.href = '/';
            });

            //POST FORM แบบ 2 AJAX
            $('.submit-btn').click(function(){
                $.ajax({
                    url: '/journal/new',
                    type: 'POST',
                    cache: false,
                    encoding: "UTF-8",
                    data: {
                        journame: $('#journame').val(),
                        jourdes: $('#jourdes').val()
                    },
                    success: function(response){
                        console.log("POST :", response);
                        
                        window.location.href = '/';
                    },
                    error: function(error){
                        console.log("ERROR :", error);
                    }
                });
            });

            //Validate Input
            $('#journame,#jourdes').on('keyup', function(){
                $('.submit-btn').attr("disabled", function(){
                   if(  
                        $('#journame').val() === '' || 
                        $('#jourdes').val()     === ''
                    )   return true;
                    return false;
                });
                
            });
        });

    </script>
@endsection

   

