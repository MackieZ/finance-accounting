@extends('headbar')

@section('content-header')
    <div class="container-fluid content-header">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 offset-md-3 offset-sm-3">
                    <img src="https://cdn.iconscout.com/public/images/icon/premium/png-512/donation-3fc3de48f7057880-512x512.png" class="img-header"/>
                </div>
                <div hidden class="col-md-6 order-md-1 text-center text-md-left pr-md-5 margin-top20 welcome-word">
                    <h1 class="mb-3 bd-text-purple-bright">Smilectronics Corporation</h1>
                    <p class="lead">
                        For Your Business
                    </p>
                    <p class="lead mb-4" id="text-head">
                        We believe numbers without meaning are useless. <br> 
                        Our mission is to become your trusted business advisor. <br>
                        Our Certified Accountants are passionate about your success.
                    </p>
                        <button class="btn btn-custom-color" id="learnmore">Learn more</button>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- Simple Forms Section -->
    <div class="container-fluid analysis-head">
        <div class="col-md-6 flex-center analysis-head-left" >
            <div class="row">
                <div class="row flex-center" id="assets"></div>
                <div class="row description">Total Assets</div>
            </div>
        </div>
        <div class="col-md-6 flex-center analysis-head-right">
            <div class="row">
                <div class="row flex-center" id="account"></div>
                <div class="row description">Accounts</div>
            </div>
        </div>
    </div>
    <!-- Simple Forms Section -->
    <div class="container-fluid simple-form-head">
        Simple Forms
    </div>
    <div class="container-fluid simple-form-section">
        <div class="row" style="margin: 0px">
            <div class="col col-md-3" style="padding: 0px 0px">
                <a href="/product" class="block block-1">
                    <div class="row block-title">
                        SIMPLE FORM
                    </div>
                    <div class="row block-subtitle">
                        ADD PRODUCT
                    </div>
                    <img src="https://image.flaticon.com/icons/svg/308/308833.svg" alt="" class="img-icon-block">
                </a>
            </div>
            <div class="col col-md-3" style="padding: 0px 0px">
                <a href="/staff" class="block block-2">
                    <div class="row block-title">
                        SIMPLE FORM
                    </div>
                    <div class="row block-subtitle">
                        ADD STAFF
                    </div>
                    <img src="https://image.flaticon.com/icons/svg/308/308861.svg" alt="" class="img-icon-block">
                </a>
            </div>
            <div class="col col-md-3" style="padding: 0px 0px">
                <a href="/company" class="block block-1">
                    <div class="row block-title">
                        SIMPLE FORM
                    </div>
                    <div class="row block-subtitle">
                        ADD COMPANY
                    </div>
                    <img src="https://image.flaticon.com/icons/svg/306/306464.svg" alt="" class="img-icon-block">
                </a>
            </div>
            <div class="col col-md-3" style="padding: 0px 0px">
                <a href="/account" class="block block-2">
                    <div class="row block-title">
                        SIMPLE FORM
                    </div>
                    <div class="row block-subtitle">
                        ADD ACCOUNT
                    </div>
                    <img src="https://image.flaticon.com/icons/svg/306/306443.svg" alt="" class="img-icon-block">
                </a>
            </div>
        </div>
    </div>
    <!-- Complex form section -->
    <div class="container-fluid complex-form-head">
        Complex Forms
    </div>
    <div class="container-fluid complex-form-section">
        <div class="row" style="margin: 0px">
            <div class="col col-md-3" style="padding: 0px 0px">
                <a href="/account-complex" class="block block-1">
                    <div class="row block-title">
                        COMPLEX FORM
                    </div>
                    <div class="row block-subtitle">
                        EDIT ACCOUNT
                    </div>
                    <img src="https://image.flaticon.com/icons/svg/308/308836.svg" alt="" class="img-icon-block">
                </a>
            </div>
            <div class="col col-md-3" style="padding: 0px 0px">
                <a href="/journalEntry" class="block block-2">
                    <div class="row block-title">
                        COMPLEX FORM
                    </div>
                    <div class="row block-subtitle">
                        JOURNAL ENTRY
                    </div>
                    <img src="https://image.flaticon.com/icons/svg/308/308827.svg" alt="" class="img-icon-block">
                </a>
            </div>
            <div class="col col-md-3" style="padding: 0px 0px">
                <a href="/paymentInvoice" class="block block-1">
                    <div class="row block-title">
                        COMPLEX FORM
                    </div>
                    <div class="row block-subtitle">
                        PAYMENT INVOICE
                    </div>
                    <img src=" https://image.flaticon.com/icons/svg/308/308832.svg" alt="" class="img-icon-block">
                </a>
            </div>
            <div class="col col-md-3" style="padding: 0px 0px">
                <a href="/StaffComplex" class="block block-2">
                    <div class="row block-title">
                        COMPLEX FORM
                    </div>
                    <div class="row block-subtitle">
                        ADD STAFF
                    </div>
                    <img src="https://image.flaticon.com/icons/svg/309/309654.svg" alt="" class="img-icon-block">
                </a>
            </div>
        </div>
    </div>
    <!-- Complex form section -->
    <div class="container-fluid simple-form-head">
        Analysis Report
    </div>
    <div class="container-fluid simple-form-section">
        <div class="row" style="margin: 0px">
            <div class="col col-md-3" style="padding: 0px 0px">
                <a href="/trendanalysis" class="block block-1">
                    <div class="row block-title">
                        ANALYSIS REPORT
                    </div>
                    <div class="row block-subtitle">
                        TREND ANALYSIS
                    </div>
                    <img src="https://image.flaticon.com/icons/svg/306/306466.svg" alt="" class="img-icon-block">
                </a>
            </div>
            <div class="col col-md-3" style="padding: 0px 0px">
                <a href="/balanceSheet" class="block block-2">
                    <div class="row block-title">
                        ANALYSIS REPORT
                    </div>
                    <div class="row block-subtitle">
                        BALANCE SHEET
                    </div>
                    <img src="https://image.flaticon.com/icons/svg/309/309656.svg" alt="" class="img-icon-block">
                </a>
            </div>
            <div class="col col-md-3" style="padding: 0px 0px">
                <a href="/incomeStatement" class="block block-1">
                    <div class="row block-title">
                        ANALYSIS REPORT
                    </div>
                    <div class="row block-subtitle">
                        INCOME STATEMENT
                    </div>
                    <img src="https://image.flaticon.com/icons/svg/308/308843.svg" alt="" class="img-icon-block">
                </a>
            </div>
            <div class="col col-md-3" style="padding: 0px 0px">
                <a href="/generalJournal" class="block block-2">
                    <div class="row block-title">
                        ANALYSIS REPORT
                    </div>
                    <div class="row block-subtitle">
                        GENERAL JOURNAL
                    </div>
                    <img src="https://image.flaticon.com/icons/svg/309/309671.svg" alt="" class="img-icon-block">
                </a>
            </div>
        </div>
        <!-- ROW 1 -->
        <div class="row" style="margin: 0px">
            <div class="col col-md-3" style="padding: 0px 0px">
                <a href="/generalLedger" class="block block-2">
                    <div class="row block-title">
                        ANALYSIS REPORT
                    </div>
                    <div class="row block-subtitle">
                        GENERAL LEDGER
                    </div>
                    <img src="https://image.flaticon.com/icons/svg/186/186074.svg" alt="" class="img-icon-block">
                </a>
            </div>
            <div class="col col-md-3" style="padding: 0px 0px">
                <a href="/trialBalance" class="block block-1">
                    <div class="row block-title">
                        ANALYSIS REPORT
                    </div>
                    <div class="row block-subtitle">
                        TRIAL BALANCE
                    </div>
                    <img src="https://image.flaticon.com/icons/svg/285/285073.svg" alt="" class="img-icon-block">
                </a>
            </div>
            <div class="col col-md-3" style="padding: 0px 0px">
                <a href="/recieveSheet" class="block block-2">
                    <div class="row block-title">
                        ANALYSIS REPORT
                    </div>
                    <div class="row block-subtitle">
                        RECIEVE SHEET
                    </div>
                    <img src="https://image.flaticon.com/icons/svg/308/308837.svg" alt="" class="img-icon-block">
                </a>
            </div>
            <div class="col col-md-3" style="padding: 0px 0px">
                <a href="/accountList" class="block block-1">
                    <div class="row block-title">
                        ANALYSIS REPORT
                    </div>
                    <div class="row block-subtitle">
                        ACCOUNT LIST
                    </div>
                    <img src="https://image.flaticon.com/icons/svg/285/285075.svg" alt="" class="img-icon-block">
                </a>
            </div>
        </div>
        <!-- ROW 2 -->
        <div class="row" style="margin: 0px">
            <div class="col col-md-3" style="padding: 0px 0px">
                <a href="/commonSizeBalanceSheet" class="block block-1">
                    <div class="row block-title">
                        ANALYSIS REPORT
                    </div>
                    <div class="row block-subtitle">
                        COMMON SIZE BALANCE SHEET
                    </div>
                    <img src="https://image.flaticon.com/icons/svg/306/306445.svg" alt="" class="img-icon-block">
                </a>
            </div>
            <div class="col col-md-3" style="padding: 0px 0px">
                <a href="/invoiceSheet" class="block block-2">
                    <div class="row block-title">
                        ANALYSIS REPORT
                    </div>
                    <div class="row block-subtitle">
                        INVOICE SHEET
                    </div>
                    <img src="https://image.flaticon.com/icons/svg/308/308858.svg" alt="" class="img-icon-block">
                </a>
            </div>
            <div class="col col-md-3" style="padding: 0px 0px">
                <a href="/productSale" class="block block-1">
                    <div class="row block-title">
                        ANALYSIS REPORT
                    </div>
                    <div class="row block-subtitle">
                        PRODUCT SALE
                    </div>
                    <img src="https://image.flaticon.com/icons/svg/308/308840.svg" alt="" class="img-icon-block">
                </a>
            </div>
            <div class="col col-md-3" style="padding: 0px 0px">
                <a href="/incomeStatement" class="block block-2">
                    <div class="row block-title">
                        ANALYSIS REPORT
                    </div>
                    <div class="row block-subtitle">
                        INCOME STATEMENT
                    </div>
                    <img src="https://image.flaticon.com/icons/svg/285/285042.svg" alt="" class="img-icon-block">
                </a>
            </div>
        </div>
        <!-- ROW 3 -->
    </div>
@endsection

@section('style')
    <style>
        .content-header {
            height: auto;
            /* background-color: #fff; */
            padding: 20px 30px 100px 20px;
            background: #3a7bd5;  
            background: -webkit-linear-gradient(to right, #3a6073, #3a7bd5);  /* Chrome 10-25, Safari 5.1-6 */
            background: linear-gradient(to right, #3a6073, #3a7bd5);
            color: #ffffff;       
            box-shadow: 2px 0px 12px rgba(1a,40,43,0.8);
        }

        @media (max-width: 376px){
            .content-header {
                padding: 150px 40px 100px 40px !important;
            }
        }

        hr {
            margin: 20px 0px 5px 0px;
            border: 1px solid #3a7bd5;
        }

        .simple-form-section {
            padding: 0px 0px;
            background-color: #FFFFFF;
        }

        .complex-form-section {
            padding: 0px 0px;
            background-color: #FFFFFF;
        }

        .img-header {
            border-radius: 50%; 
            border: 2px solid #ffffff;
            /* margin: 50px auto; */
            position: relative;
            display: flex;
            justify-content: center;
            width: 350px;
            height: 350px;
            align-self: center;
            margin-top: 75px;
            margin-left: auto;
            margin-right: auto;

            left: -768px;
        }

        .img-product {
            border-radius: 50%;
            border: 1px solid #3a7bd5;
        }

        .welcome-word {
            padding-top: 60px;

            /* bottom: -360px; */
        }

        .btn-custom-color {
            background-color: #E06D37;
            width: 150px;
            height: 50px;
        }

        .img-icon-block {
            width: 125px;
            height: 125px;
            align-self: center;
            justify-content: center;
            text-align: center;
            margin-left: auto;
            margin-right: auto;
        }

        .simple-form-head {
            display: flex;
            justify-content: center;
            align-items: center;
            width: 100%;
            padding: 175px 0px;
            background-color: #2c2c2c;
            color: #EFEFEF;
            font-size: 2.75rem;
        }

        .analysis-head-right {
            margin: 25px 0px;
            padding: 25px 0px;
        }
        
        .analysis-head-left {
            border-right: 1px solid #3a7bd5;
            margin: 25px 0px;
            padding: 25px 0px;
        }

        .analysis-head {
            line-height: 1.35;
            font-weight: 500;
            display: flex;
            justify-content: center;
            align-items: center;
            width: 100%;
            padding: 0px;
            background-color: #EFEFEF;
            color: #3a7bd5;
            font-size: 2.75rem;
        }

        .description {
            text-align: center;
            font-weight: 300;
            font-size: 28px;
            color: #747474;
        }

        .complex-form-head {
            display: flex;
            justify-content: center;
            align-items: center;
            width: 100%;
            padding: 175px 0px;
            background-color: #3a7bd5;
            color: #EFEFEF;
            font-size: 2.75rem;
        }

        .block {
            display: flex;
            /* position: relative; */
            /* overflow: hidden; */
            text-align: center;
            
        }

        .block:hover {
            /* background-color: #FFFFFF; */
            transform: scale(0.95);
            transition: .8s;
        }

        .block-title {
            position: absolute;
            left: 2rem;
            top: 1rem;
            font-weight: 600;
            color: #3a7bd5;
            font-size: 0.8rem;
            /* display: inline-block; */
        }

        .block-subtitle {
            position: absolute;
            left: 2rem;
            top: 2rem;
            font-size: 0.75rem;
            font-weight: 500;
            color: #585858;
        }

        .simple-form-section > div.row > div > a.block-1 {
            background-color: #4A4A4A;
            height: 20rem;
            padding: 1.25rem;
        }

        .simple-form-section > div.row > div > a.block-2 {
            background-color: #585858;
            height: 20rem;
            padding: 1.25rem;
        }

        .simple-form-section > div.row > div > a > div.block-title {
            color: #5a9be5 !important;
        }

        .simple-form-section > div.row > div > a > div.block-subtitle {
            color: #ffffff !important;
        }

        .complex-form-section > div.row > div > a.block-1 {
            background-color: #EFEFEF;
            height: 20rem;
            padding: 1.25rem;
        }

        .complex-form-section > div.row > div > a.block-2 {
            background-color: #E0E0E0;
            height: 20rem;
            padding: 1.25rem;
        }

    </style>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $(".welcome-word").hide();
            $('.img-header').animate({left: '20px'}, "slow");
            setTimeout(function () {
                $(".welcome-word").fadeIn("slow");
            }, 600);
            $('#learnmore').on('click',function() {
                window.scrollTo(500, 300);
                // $('html,body').scrollTo(500,200);
            });

            $.ajax({
                    url: '/Home2',
                    type: 'GET',
                    cache: false,
                    encoding: "UTF-8",
                    success: function(response){
                        console.log("GET :", response);
                        
                        $('#assets').text('฿ ' + response);
                       // window.location.href = '/';
                    },
                    error: function(error){
                        console.log("ERROR :", error);
                    }
                });
                
            $.ajax({
                url: '/Home3',
                type: 'GET',
                cache: false,
                encoding: "UTF-8",
                success: function(response){
                    console.log("GET :", response);
                    
                    $('#account').text(response);
                    // window.location.href = '/';
                },
                error: function(error){
                    console.log("ERROR :", error);
                }
            });

        });
    </script>
@endsection