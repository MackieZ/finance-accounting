@extends('headbar')

@section('content')
    <div class="content-form">
    <div class="container">
    <div class="row">
        <h3>General Journal</h3>
    </div>
    <div class="row">
        <p>Period {{$start}} To {{$end}}</p>
        
    </div>
    <div class="row">
    <table class="table table-bordered">
    <thead>
      <tr>
        <th scope="col">Date</th>
        <th scope="col">Description</th>
        <th scope="col">AccountID</th>
        <th scope="col">Debit</th>
        <th scope="col">Credit</th>
      </tr>
    </thead>
    <tbody>
    @foreach($invoice as $value)
    <tr>
        <th scope="row" rowspan="{{count($value->data)+1}}">{{$value->InvoiceDate}}</th>
    </tr>
       @foreach($value->data as $data)
       <tr>
        <td>{{$data->AccountName}}</td>
        <td>{{$data->AccountID}}</td>
        <td>{{$data->Debit}}</td>
        <td>{{$data->Credit}}</td>
        </tr>
        @endforeach
    
    @endforeach
    </tbody>
    </table>
    </div>
    </div>
    </div>
    
@endsection

