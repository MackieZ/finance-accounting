@extends('headbar')

@section('content')
    <div class="content-form">
    <div class="container">
    <div class="row">
        <h3>Balance Sheet</h3>
    </div>
   
    <div class="row">
    <table class="table table-hover" >
      <thead>
        <tr>
      
        <th style="padding-right:50%; font-size:18px;">Asset</th>
        <th > </th>
     
        </tr>
      </thead>
    <tbody>
  @foreach($asset as $value)
    @foreach($value as $test)
    <tr >
      <td style="font-size:15px;">{{$test->name}}</td>
      <td style="font-size:15px;">{{$test->debit}}</td>  
    </tr>
    @endforeach
    @endforeach
   
    <tr >
      <td style="font-size:15px;">Total Asset</td>
      <td style="font-size:15px;">{{$ta}}</td>  
    </tr>
    
  </tbody>
  <thead>
    <tr> <td style="font-size:19px"> Liabilities & Equity </td> </tr>
  </thead>
    <tr>
      <th style="padding-left:10%; padding-right:50%; font-size:16px;">Liabilities</th>
      <th > </th>
    </tr>
  <tbody>
  @foreach($liability as $value)
    @foreach($value as $test)
    <tr >
      <td style="font-size:15px;">{{$test->name}}</td>
      <td style="font-size:15px;">{{$test->debit}}</td>  
    </tr>
    @endforeach
    @endforeach     
    <tr >
      <td style="font-size:15px;">Total Liabilities</td>
      <td style="font-size:15px;">{{$tl}}</td>  
    </tr>
  </tbody>
  
  <tr>
      <th style="padding-left:10%; padding-right:50%; font-size:16px;">Equity</th>
      <th > </th>
    </tr>
  <tbody>
  @foreach($equity as $value)
    @foreach($value as $test)
    <tr >
      <td style="font-size:15px;">{{$test->name}}</td>
      <td style="font-size:15px;">{{$test->debit}}</td>  
    </tr>
    @endforeach
    @endforeach
    <tr >
      <td style="font-size:15px;">Total Equity</td>
      <td style="font-size:15px;">{{$te}}</td>  
    </tr>
  </tbody>
  <tr><th></th></tr>
  <thead>
    
    <tr >
      <td style="font-size:18px;">Total</td>
      <td style="font-size:15px;">{{$total}}</td>  
    </tr>
   
  </thead>
</table>

    
    </div>
    </div>
    </div>
    
@endsection

