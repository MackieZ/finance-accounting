@extends('headbar')

@section('content')
    <!-- <form method = "post" action="/product/new">  -->  <!-- POST FORM แบบ 1 (PHP) -->
    <div class="content-form">
        <div class="container">
            <div class="row title">
                <span class="glyphicon glyphicon-plus-sign" style="margin-right: 1rem"></span>New Product
                <hr>
            </div>
            <div class="form-group">
                <label for="productName">Product Name</label>
                <input type="text" class="form-control" id="productName" placeholder="Name">
                <!-- 
                    ถ้าจะ POST FORM แบบ 1 ใส่ name="productName" ด้วย 
                    <input type="text" class="form-control" name="productName" id="productName" placeholder="Name">
                -->
            </div>
            <div class="form-group">
                <label for="productDescription">Product Description</label>
                <input type="text" class="form-control" id="productDescription" placeholder="Description">
            </div>
            <div class="form-group">
                <label for="productPrice">Product Price</label>
                <input type="number" class="form-control" id="productPrice" placeholder="Price">
                <small class="form-text text-muted">- Thai Baht</small>
            </div>
            <div class="form-group">
                <label for="productQuantity">Product Quantity</label>
                <input type="number" class="form-control" id="productQuantity" placeholder="0">
                <small class="form-text text-muted">- Amount of Product</small>
            </div>
            <div class="row margin-top20">
                <div class="col-12 col-md-12 margin-top20">
                    <button disabled type="button" class="btn btn-primary submit-btn">Submit</button>
                    <button type="button" class="btn btn-secondary cancel-btn">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!-- </form> POST FORM แบบ 1 (PHP) -->
@endsection

@section('style')
    <style>
        .title {
            font-size: 28px; 
            margin: 20px 0px;  
            color: #3097D1;
        }
        hr {
            border: 1px solid #3097D1;
            margin: 15px 0px 10px 0px;
        }
    </style>
@endsection

@section('script')
    <script>
        $(document).ready(function (){
            // POST FORM แบบ 2 AJAX
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.cancel-btn').click(function(){
                window.location.href = '/';
            });

            //POST FORM แบบ 2 AJAX
            $('.submit-btn').click(function(){
                $.ajax({
                    url: '/product/new',
                    type: 'POST',
                    cache: false,
                    encoding: "UTF-8",
                    data: {
                        productName: $('#productName').val(),
                        productDescription: $('#productDescription').val(),
                        productPrice: $('#productPrice').val(),
                        productQuantity: $('#productQuantity').val()
                    },
                    success: function(response){
                        console.log("POST :", response);
                        
                        window.location.href = '/';
                    },
                    error: function(error){
                        console.log("ERROR :", error);
                    }
                });
            });

            //Validate Input
            $('#productName, #productDescription, #productPrice, #productQuantity').on('keyup', function(){
                $('.submit-btn').attr("disabled", function(){
                    if( $('#productName').val() === '' || 
                        $('#productDescription').val() === '' || 
                        $('#productPrice').val() === '' || 
                        $('#productQuantity').val() === ''
                    )   return true;
                    return false;
                });
            });
        });

    </script>
@endsection
