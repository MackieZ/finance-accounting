@extends('headbar')

@section('content')
    <div class="content-form">
    <div class="container">
    <div class="row">
        <h3>Account List</h3>
    </div>
     
    <div class="row">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">AccountNo.</th>
                    <th scope="col">Account Name</th>
                    <th scope="col">AccountList</th>
                    <th scope="col">Balance</th>
                </tr>
            </thead>
            <tbody>
                @foreach($account as $data)
                <tr>
                <td>{{$data->AccountID}}</td>
                <td>{{$data->AccountName}}</td>
                <td>{{$data->accountType}}</td>
                <td>{{$data->amount}}</td>
                </tr>
            @endforeach
            
            </tbody>

        </table>
    </div>

    </div>
    </div>
    </div>
    
@endsection

