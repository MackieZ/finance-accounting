<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Finance and Accounting</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600" rel="stylesheet" type="text/css">
    <link href="/css/app.css" rel="stylesheet" type="text/css">
    @yield('header')
</head>
    <style>
        html, body {
            color: #cecece;
            background: #262626;
            font-family: 'Raleway', sans-serif;
            font-weight: 500;
            height: 100vh;
            margin: 0;
            font-size: 18px;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .margin-horizontal-auto {
            margin-left: auto;
            margin-right: auto;
        }

        .margin-vertical-auto {
            margin-top: auto;
            margin-bottom: auto;
        }

        .margin-left2 {
            margin-left: 2px;
        }

        .margin-left5 {
            margin-left: 5px;
        }
        
        .margin-left10 {
            margin-left: 10px;
        }

        .margin-left20 {
            margin-left: 20px;
        }

        .margin-right2 {
            margin-right: 2px;
        }

        .margin-right5 {
            margin-right: 5px;
        }
        
        .margin-right10 {
            margin-right: 10px;
        }

        .margin-right20 {
            margin-right: 20px;
        }

        .margin-top2 {
            margin-top: 2px;
        }

        .margin-top5 {
            margin-top: 5px;
        }
        
        .margin-top10 {
            margin-top: 10px;
        }

        .margin-top20 {
            margin-top: 20px;
        }

        .margin-bottom2 {
            margin-bottom: 2px;
        }

        .margin-bottom5 {
            margin-bottom: 5px;
        }
        
        .margin-bottom10 {
            margin-bottom: 10px;
        }

        .margin-bottom20 {
            margin-bottom: 20px;
        }

        .content-form {
            margin: 35px 0px;
            padding: 15px 40px 40px 40px;
            background-color: #efefef;
            color: #3f3f3f;
            height: auto;
            min-height: 500px;
            width: 678px;
            display: flex;
            justify-content: center;
            border-radius: 15px;
            border: 2px solid #36393f;
            /* box-shadow: 0px 0px 0px 3px #efefef; */
        }

        .navbar {
            /* padding: 20px 0px; */
            padding: 5px;
            position: relative;
            background-color: #1c1c1c;
            height: 60px;
            margin: 0px;
            border: 0px 0px 1px 0px solid #26292f;
            box-shadow: 0px 1px 15px 1px #454545;
        }

        .navbar-nav > li.active-navbar > a {
            color: #ffffff;
        }
        
        .navbar-inverse .navbar-nav > .open > a, li.open > a:focus {
            background-color: transparent !important;
        }

        .submit-btn, .cancel-btn {
            width: 100px;
        }

        small {
            font-size: 14px;
        }

        .footer {
            padding: 60px 100px;
            background-color: #3a7bd5;
            
        }

        .icon-footer {
            border-radius: 50%;
            
            width: 75px;
            height: 75px;
            /* display: inline-block; */
            background-color: #83aae0;
            /* opacity: .4; */
            margin: 0px 10px;
        }

        .icon-footer:hover {
            cursor: pointer;
            transform: scale(1.1);
            transition .3s;
            border: 3px solid white;
            
        }

        .img-icon {
            text-align: center;
            width: 35px;
            height: 35px;
            /* background-color: #aaaaaa; */
            opacity: 1 !important;
            margin-left: 20px;
            margin-right: 20px;
            /* position: relative; */
        }

        .subfooter {
            height: 40px;
            width: 100%;
        }

        .panel-body, .panel-heading {
            background-color: #4c4c4c !important;
            color: #cecece !important;
        }

        .navbar-nav > li.active-navbar > a {
            color: #9d9d9d !important;
        }

        .navbar-brand {
            color: #ffffff !important;
        }

    </style>
    @yield('style')
    
<body>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
            <a class="navbar-brand" href="/">Smilectronics Coporation</a>
            </div>
            <ul class="nav navbar-nav">
            <li class="active-navbar"><a href="/">Home</a></li>
            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Simple Forms <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="/product">Product</a></li>
                    <li><a href="/company">Company</a></li>
                    <li><a href="/staff">Staff</a></li>
                    <li><a href="/account">Account</a></li>
                </ul>
            </li>
            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Complex Forms <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="/account-complex">Edit Account</a></li>
                    <li><a href="/journalEntry">Journal Entry</a></li>
                    <li><a href="/paymentInvoice">Payment Invoice</a></li>
                    <li><a href="/StaffComplex">Add Staff</a></li>
                </ul>
            </li>
            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Analysis <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="/trendanalysis">Trend Analysis</a></li>
                    <li><a href="/balanceSheet">BalanceSheet</a></li>
                    <li><a href="/incomeStatement">IncomeStatement</a></li>
                    <li><a href="/generalJournal">GeneralJournal</a></li>
                    
                    <li><a href="/generalLedger">GeneralLedger</a></li>
                    <li><a href="/trialBalance">TrialBalance</a></li>
                    <li><a href="/recieveSheet">RecieveSheet</a></li>
                    <li><a href="/accountList">AccountList</a></li>
                
                    <li><a href="/commonSizeBalanceSheet">CommonSizeBalanceSheet</a></li>
                    <li><a href="/invoiceSheet">InvoiceSheet</a></li>
                    <li><a href="/productSale">ProductSale</a></li>
                    <li><a href="/incomeStatement">IncomeStatement</a></li>
                    
                </ul>
            </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
            @guest
                <li><a href="{{ route('register') }}">Register</a></li>
                <li><a href="{{ route('login') }}">Login</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                @endguest
            </ul>
        </div>
    </nav>
    @yield('content-header')
    <div class="container flex-center" style="margin-top: 45px">
            @yield('content')
        </div>
    </div>
    
</body>
<script src="/js/app.js"></script>
@yield('script')
</html>