@extends('headbar')

@section('content')
    <div class="content-form">
    <div class="container">
    <div class="row">
        <h3>Product Sale</h3>
    </div>
    <div class="row">
        <p>period {{$start}} to {{$end}}</p>
        
    </div>
    <div class="row">
    <table class="table table-bordered">
    <thead>
      <tr>
        <th scope="col">ProductID</th>
        <th scope="col">Product Name</th>
        <th scope="col">Product Price</th>
        <th scope="col">Total Sale</th>
        <th scope="col">Total price</th>
      </tr>
    </thead>
    <tbody>
    @foreach($sale as $data)
    
       <tr>
        <td>{{$data->ProductID}}</td>
        <td>{{$data->ProductName}}</td>
        <td>{{$data->productprice}}</td>
        <td>{{$data->Amount}}</td>
        <td>{{$data->total}}</td>
        </tr>

    
    @endforeach
    </tbody>
    </table>
    </div>
    </div>
    </div>
    
@endsection

