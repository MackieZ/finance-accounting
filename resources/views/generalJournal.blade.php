@extends('headbar')

@section('content')
    <form method = "post" action="/generalJournal/report">
    {{csrf_field()}}
        <div class="content-form">
            <div class="container">
                <div class="row">
                    <h3>General Journal</h3>
                </div>
                <div class="row">
                    <div class="form-group">
                    <label for="cpn">Period : </label>
                    <input type="date" name="start">
                    To
                    <input type="date" name="end">
                </div>
                <button class="btn">Click</button>
                <br>
            </div>        
        </div>
    </form>
@endsection
