@extends('headbar')

@section('content')
    <form method = "post" action="editJournal/submit">
    {{csrf_field()}}
        <div class="content-form">
            <div class="container">
                <div class="row">
                    <h3>edit Journal</h3>
                </div>
                <div class="row">
                        <div class="form-group">
                            <select name="JournalID" id="journalID">
                            @foreach($journal as $value)
                                <option value= '{{$value->JournalID}}'> ID:{{$value->JournalID}} Name:{{$value->JournalName}} </option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                <button class="btn" name="submit" value=1>Edit</button>
                <button class="btn" name="submit" value=2>Delete</button>
                <br>
                
            </div>        
        </div>
    </form>

@endsection

