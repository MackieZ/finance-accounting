@extends('headbar')

@section('content')
    <!-- <form method = "post" action="/product/new">  -->  <!-- POST FORM แบบ 1 (PHP) -->
    <div class="content-form">
        <div class="container">
            <div class="row title">
                <span class="glyphicon glyphicon-plus-sign" style="margin-right: 1rem"></span>Add Staff
                <hr>
            </div>
            <div class="form-group">
                <label for="stfirstname">First Name</label>
                <input type="text" class="form-control" id="stfirstname" placeholder="First Name">
            </div>
            <div class="form-group">
                <label for="stlastname">Last Name</label>
                <input type="text" class="form-control" id="stlastname" placeholder="Last Name">
            </div>
            <div class="form-group">
                <label for="stphone">Phone Number</label>
                <input type="number" class="form-control" id="stphone" placeholder="phone number 10 digit ex. 0871015783" >
            </div>
            <div class="form-group">
                <label for="staddress">Address</label>
                <input type="text" class="form-control" id="staddress" placeholder="Address">
            </div>
            <div class="form-group">
                <label for="stdepid">Department ID</label>
                <input type="number" class="form-control" id="stdepid" placeholder="Department ID As 351">
            </div>
            <div class="form-group">
                <label for="staccid">Account ID</label>
                <input type="number" class="form-control" id="staccid" placeholder="Account ID As 1234">
            </div>
             <div class="form-group">
                <label for="stcom">Company ID</label>
                <input type="number" class="form-control" id="stcom" placeholder="Company ID As 1234">
            </div>
            <div class="row margin-top20">
                <div class="col-12 col-md-12 margin-top20">
                    <button disabled type="button" class="btn btn-primary submit-btn">Submit</button>
                    <button type="button" class="btn btn-secondary cancel-btn">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!-- </form> POST FORM แบบ 1 (PHP) -->
@endsection

@section('style')
    <style>
        .title {
            font-size: 28px; 
            margin: 20px 0px;  
            color: #3097D1;
        }
        hr {
            border: 1px solid #3097D1;
            margin: 15px 0px 10px 0px;
        }
    </style>
@endsection

@section('script')
    <script>
        $(document).ready(function (){
            // POST FORM แบบ 2 AJAX
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.cancel-btn').click(function(){
                window.location.href = '/';
            });

            //POST FORM แบบ 2 AJAX
            $('.submit-btn').click(function(){
                $.ajax({
                    url: '/staff/new',
                    type: 'POST',
                    cache: false,
                    encoding: "UTF-8",
                    data: {
                        stfirstname: $('#stfirstname').val(),
                        stlastname: $('#stlastname').val(),
                        stphone: $('#stphone').val(),
                        staddress: $('#staddress').val(),
                        stdepid: $('#stdepid').val(),
                        staccid: $('#staccid').val(),
                         stcom: $('#stcom').val()
                    },
                    success: function(response){
                        console.log("POST :", response);
                        
                        window.location.href = '/';
                    },
                    error: function(error){
                        console.log("ERROR :", error);
                    }
                });
            });

            //Validate Input
            $('#stfirstname, #stlastname, #stphone,#staddress,#stdepid,#staccid,#stcom').on('keyup', function(){
                $('.submit-btn').attr("disabled", function(){
                   if(  
                        $('#stfirstname').val() === '' || 
                        $('#stfirstname').val() === '' || 
                        $('#stphone').val()     === '' || 
                        $('#staddress').val()   === '' || 
                        $('#stdepid').val()     === '' || 
                        $('#staccid').val()     === ''||
                        $('#stcom').val()     === '' 
                    )   return true;
                    return false;
                });
                
            });
        });

    </script>
@endsection
