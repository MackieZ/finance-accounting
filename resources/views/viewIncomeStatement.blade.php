@extends('headbar')

@section('content')
    <div class="content-form">
    <div class="container">
    <div class="row">
        <h3>Income Statement</h3>
    </div>
    <div class="row">
        <p>Period : {{$start}} To {{$end}}</p>
    </div>
    <div class="row">
    <table class="table table-hover" >
  <thead >
    <tr>
      
      <th style="padding-right:50%; font-size:18px;">Revenue</th>
      <th > </th>
     
    </tr>
  </thead>
  <tbody>
  @foreach($revenue as $value)
    @foreach($value as $test)
    <tr >
      <td style="font-size:15px;">{{$test->name}}</td>
      <td style="font-size:15px;">{{$test->debit}}</td>  
    </tr>
    @endforeach
    @endforeach
   
    <tr >
      <td style="font-size:15px;">Total Revenue</td>
      <td style="font-size:15px;">{{$totalRevenue}}</td>  
    </tr>
    
  </tbody>
  <thead >
    <tr>
      
      <th style="padding-right:50%; font-size:18px;">Expense</th>
      <th > </th>
     
    </tr>
  </thead>
  <tbody>
  @foreach($expense as $value)
    @foreach($value as $test)
    <tr >
      <td style="font-size:15px;">{{$test->name}}</td>
      <td style="font-size:15px;">{{$test->debit}}</td>  
    </tr>
    @endforeach
    @endforeach
   
    <tr >
      <td style="font-size:15px;">Total Expense</td>
      <td style="font-size:15px;">{{$totalExpense}}</td>  
    </tr>
    
  </tbody>
  
  <thead >
    <tr>
      
      <th style="padding-right:50%; font-size:18px;">Net Income</th>
      <th >{{$net}}</th>
     
    </tr>
  </thead>

</table>
    
    </div>
    </div>
    </div>
    
@endsection

