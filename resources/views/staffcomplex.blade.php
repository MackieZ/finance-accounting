@extends('headbar')

@section('content')
<!-- <form method = "post" action="/staffcomplex/new">  -->
    <div class="content-form">
        <div class="container">
            <div class="row title">
                <span class="glyphicon glyphicon-plus-sign" style="margin-right: 1rem"></span>Add Staff
                <hr>
            </div>
            <span>Company: </span>
                     <select id="cid">
                     <option value="0" disabled="true" selected="true">Company Name</option>
                        @foreach($companyname as $cname)
                            <option value="{{$cname->CompanyID}}">{{$cname->CompanyName}}</option>
                         @endforeach
                     </select>
            <a href="/company">+ new company</a>   
           <div class="form-group">
                <label for="stfirstname">First Name</label>
                <input type="text" class="form-control" id="stfirstname" placeholder="First Name">
            </div>
            <div class="form-group">
                <label for="stlastname">Last Name</label>
                <input type="text" class="form-control" id="stlastname" placeholder="Last Name">
            </div>
            <div class="form-group">
                <label for="stphone">Phone Number</label>
                <input type="number" class="form-control" id="stphone" placeholder="phone number 10 digit ex. 0871015783" >
            </div>
            <div class="form-group">
                <label for="staddress">Address</label>
                <input type="text" class="form-control" id="staddress" placeholder="Adress">
            </div>
            <span>Department: </span>
                     <select  id="did">
                     <option value="0" disabled="true" selected="true">Department Name</option>
                        @foreach($departmentname as $dname)
                            <option value="{{$dname->DepartmentID}}">{{$dname->DepartmentName}}</option>
                         @endforeach
                     </select>

            <br><span>Account : </span>
                     <select id="aid">
                     <option value="0" disabled="true" selected="true">Account Name</option>
                        @foreach($accountname as $aname)
                            <option value="{{$aname->AccountID}}">{{$aname->AccountName}}</option>
                         @endforeach
                     </select>   
            <a href="/account">+ new account</a>     
            <div class="row margin-top20">
                <div class="col-12 col-md-12 margin-top20">
                    <button disabled type="button" class="btn btn-primary submit-btn">Submit</button>
                    <button type="button" class="btn btn-secondary cancel-btn">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!-- </form> POST FORM แบบ 1 (PHP) -->
@endsection

@section('style')
    <style>
        .title {
            font-size: 28px; 
            margin: 20px 0px;  
            color: #3097D1;
        }
        hr {
            border: 1px solid #3097D1;
            margin: 15px 0px 10px 0px;
        }
    </style>
@endsection
 @section('script')
    <script>
        $(document).ready(function (){
            // POST FORM แบบ 2 AJAX
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.cancel-btn').click(function(){
                window.location.href = '/';
            });
            //POST FORM แบบ 2 AJAX
            $('.submit-btn').click(function(){
                $.ajax({
                    url: '/StaffComplex/new',
                    type: 'POST',
                    cache: false,
                    encoding: "UTF-8",
                    data: {
                        stfirstname: $('#stfirstname').val(),
                        stlastname: $('#stlastname').val(),
                        stphone: $('#stphone').val(),
                        staddress: $('#staddress').val(),
                        did: $('#did').val(),
                        aid: $('#aid').val(),
                        cid: $('#cid').val()
                    },
                    success: function(response){
                        console.log("POST :", response);
                        
                        window.location.href = '/';
                    },
                    error: function(error){
                        console.log("ERROR :", error);
                    }
                });
            });

            //Validate Input
            $('#stfirstname, #stlastname, #stphone,#staddress').on('keyup', function(){
                $('.submit-btn').attr("disabled", function(){
                   if(  
                        $('#stfirstname').val() === '' || 
                        $('#stlastname').val()  === ''  || 
                        $('#stphone').val()     === '' || 
                        $('#staddress').val()   === '' 
                    )   return true;
                    return false;
                });
                
            });
        });

    </script>
@endsection
