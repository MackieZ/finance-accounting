@extends('headbar')

@section('content')
    <!-- <form method = "post" action="/product/new">  -->  <!-- POST FORM แบบ 1 (PHP) -->
    <div class="content-form">
        <div class="container">
            <div class="row title">
                <span class="glyphicon glyphicon-plus-sign" style="margin-right: 1rem"></span>Edit Staff
                <hr>
            </div>
                <div class="row">
                <span>Staff ID: </span>
                     <select id="oldsid">
                        @foreach($thisstaff as $tid)
                            <option value="{{$tid->StaffID}}">{{$tid->StaffID}}</option>
                         @endforeach
                     </select>
                     <br>
                 <!-- First Name -->
                    <span>Staff Old First Name: </span>
                     <select id="old">
                     <option value="{{$tid->StaffID}}">{{$tid->StaffFirstName}}</option>
                     </select>
                      <div class="form-group">
                       <span>New First Name</span>
                        <input type="text" class="form-control" id="stfirstname" placeholder="First Name">
                      </div>
                 <!-- Last Name -->
                  <span>Staff Old Last Name: </span>
                     <select id="old">
                     <option value="{{$tid->StaffID}}">{{$tid->StaffLastName}}</option>
                     </select>
                      <div class="form-group">
                       <span>New Last Name</span>
                        <input type="text" class="form-control" id="stlastname" placeholder="Last Name">
                      </div>
                <!-- Staff Phone -->
                  <span>Old Phone Number: </span>
                     <select id="old">
                     <option value="{{$tid->StaffID}}">{{$tid->	StaffPhoneNumber}}</option>
                     </select>
                      <div class="form-group">
                       <span>New Phone Number</span>
                        <input type="number" class="form-control" id="stphone" placeholder="Phone Number">
                      </div> 
                <!-- Staff Add ress -->
                  <span>Old Adr
                  ess: </span>
                     <select id="old">
                     <option value="{{$tid->StaffID}}">{{$tid->	StaffAddress}}</option>
                     </select>
                      <div class="form-group">
                       <span>New Address</span>
                        <input type="text" class="form-control" id="staddr" placeholder="Address">
                      </div>
                <!-- Staff Depart -->
                  <span>Old Department ID: </span>
                     <select id="old">
                     <option value="{{$tid->DepartmentID}}">{{$tid->DepartmentID}}</option>
                     </select>
                 <span> &nbsp; New Department: </span>
                     <select  id="did">
                      <option value="0" disabled="true" selected="true">Deparment Name</option>
                        @foreach($departmentname as $dname)
                            <option value="{{$dname->DepartmentID}}">{{$dname->DepartmentName}}</option>
                         @endforeach}
                     </select>
                     <br>
                         <!-- Staff Account -->
                  <span>Old Account ID: </span>
                     <select id="old">
                     <option value="{{$tid->DepartmentID}}">{{$tid->DepartmentID}}</option>
                     </select>
                 <span> &nbsp; New Account : </span>
                     <select  id="aid">
                      <option value="0" disabled="true" selected="true">Account Name</option>
                        @foreach($accountname as $aname)
                             <option value="{{$aname->AccountID}}">{{$aname->AccountName}}</option>
                         @endforeach}
                     </select>
                      <br>
                <!-- Staff Company -->
                  <span>Old Company ID: </span>
                     <select id="old">
                     <option value="{{$tid->CompanyID}}">{{$tid->CompanyID}}</option>
                     </select>
                 <span> &nbsp; New Company : </span>
                     <select  id="cid">
                      <option value="0" disabled="true" selected="true">Company Name</option>
                        @foreach($companyname as $cname)
                             <option value="{{$cname->CompanyID}}">{{$cname->CompanyName}}</option>
                         @endforeach}
                     </select>
                      
                <br>
                <button disabled type="button" class="btn btn-primary submit-btn">Submit</button>
                    <button type="button" class="btn btn-secondary cancel-btn">Cancel</button>
                <br>
                
            </div>        
        </div>
    </form>
@endsection

@section('script')
    <script>
        $(document).ready(function (){
            // POST FORM แบบ 2 AJAX
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.cancel-btn').click(function(){
                window.location.href = '/';
            });
            //POST FORM แบบ 2 AJAX
            $('.submit-btn').click(function(){
                $.ajax({
                    url: '/editstaff/putdata',
                    type: 'POST',
                    cache: false,
                    encoding: "UTF-8",
                    data: {
                        tid : $('#tid').val(),
                        stfirstname: $('#stfirstname').val(),
                        stlastname: $('#stlastname').val(),
                        stphone: $('#stphone').val(),
                        staddr: $('#staddr').val(),
                        did: $('#did').val(),
                        aid: $('#aid').val(),
                        cid: $('#cid').val()
                    },
                    success: function(response){
                        console.log("POST :", response);
                        
                        window.location.href = '/';
                    },
                    error: function(error){
                        console.log("ERROR :", error);
                    }
                });
            });

            //Validate Input
            $('#stfirstname, #stlastname, #stphone,#staddr').on('keyup', function(){
                $('.submit-btn').attr("disabled", function(){
                   if(  
                        $('#stfirstname').val() === '' || 
                        $('#stlastname').val()  === ''  || 
                        $('#stphone').val()     === '' || 
                        $('#staddr').val()   === '' 
                    )   return true;
                    return false;
                });
                
            });
        });

    </script>
@endsection
