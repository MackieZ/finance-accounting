@extends('headbar')

@section('content')
    <form method = "post" action="/journalEntry/add">
    {{csrf_field()}}
        <div class="content-form">
            <div class="container">
                <div class="row">
                    <h3>Journal Entry</h3>
                </div>
                <div class="row">
                        <div class="form-group">
                        {{ csrf_field() }}
                        <label for="name">Journal</label>
                        <input type="text" class="form-control" name="Name">
                        <label for="description">Description</label> 
                        <input type="text" class="form-control" name = "Description">
                        <label for="company">Company</label>
                        <select name="company" class="form-control">
                        @foreach($company as $data)
                        <option value="{{$data->CompanyID}}">{{$data->CompanyName}}</option>
                        @endforeach
                        </select>
            
                        <label for="date">Date</label>
                        <input type="date" class="form-control" name="date">
                        <br>
                        <label for="transaction">Transaction</label>
                        <table class="table table-bordered" id="dynamic_field">
                                    <tr>
                                        <th>Account</th>
                                        <th>Debit</th>
                                        <th>Credit</th>
                                        <th></th>
                                    </tr>  
                                    <tr>  
                                         <td><select name="account[]" class="form-control">
                                            @foreach($account as $data)
                                            <option value="{{$data->AccountID}}">{{$data->AccountName}}</option>
                                            @endforeach
                                         </select></td> 
                                         <td><input type="number" name="debit[]" style="width: 100%;"></td>
                                         <td><input type="number" name="credit[]" style="width: 100%;"></td> 
                                         <td><button type="button" name="add" id="add" class="btn btn-success">Add</button></td>  
                                    </tr>  
                        </table>  
                        
                        </div>
                    </div>
                <button class="btn">Next</button>
                <br>
                
            </div>        
        </div>
    </form>


    <script>  
 $(document).ready(function(){  
      var i=1;  
      $('#add').click(function(){  
           i++;  
           $('#dynamic_field').append('<tr id="row'+i+'"><td><select name="account[]" class="form-control">@foreach($account as $data)<option value="{{$data->AccountID}}">{{$data->AccountName}}</option>@endforeach</select></td><td><input type="number" name="debit[]" style="width: 100%;"></td><td><input type="number" name="credit[]" style="width:100%;"></td> <td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
      });  
      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  
 });  
 </script>

@endsection

