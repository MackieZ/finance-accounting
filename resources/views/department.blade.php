@extends('headbar')

@section('content')
    <!-- <form method = "post" action="/product/new">  -->  <!-- POST FORM แบบ 1 (PHP) -->
    <div class="content-form">
        <div class="container">
            <div class="row title">
                <span class="glyphicon glyphicon-plus-sign" style="margin-right: 1rem"></span>Add Department
                <hr>
            </div>
              <div class="form-group">
                <label for="depid">Department ID</label>
                <input type="number" class="form-control" id="depid" placeholder="Department ID" >
            </div>
              <div class="form-group">
                <label for="comid">Company ID</label>
                <input type="number" class="form-control" id="comid" placeholder="Company ID" >
            </div>
            <div class="form-group">
                <label for="depcode">Department Code</label>
                <input type="text" class="form-control" id="depcode" placeholder="Department Code">
            </div>
            <div class="form-group">
                <label for="depname">Department Name</label>
                <input type="text" class="form-control" id="depname" placeholder="Department Name">
            </div>
           
            <div class="row margin-top20">
                <div class="col-12 col-md-12 margin-top20">
                    <button disabled type="button" class="btn btn-primary submit-btn">Submit</button>
                    <button type="button" class="btn btn-secondary cancel-btn">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!-- </form> POST FORM แบบ 1 (PHP) -->
@endsection

@section('style')
    <style>
        .title {
            font-size: 28px; 
            margin: 20px 0px;  
            color: #3097D1;
        }
        hr {
            border: 1px solid #3097D1;
            margin: 15px 0px 10px 0px;
        }
    </style>
@endsection

@section('script')
    <script>
        $(document).ready(function (){
            // POST FORM แบบ 2 AJAX
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.cancel-btn').click(function(){
                window.location.href = '/';
            });

            //POST FORM แบบ 2 AJAX
            $('.submit-btn').click(function(){
                $.ajax({
                    url: '/department/new',
                    type: 'POST',
                    cache: false,
                    encoding: "UTF-8",
                    data: {
                        depid: $('#depid').val(),
                        comid: $('#comid').val(),
                        depcode: $('#depcode').val(),
                        depname: $('#depname').val()
                    },
                    success: function(response){
                        console.log("POST :", response);
                        
                        window.location.href = '/';
                    },
                    error: function(error){
                        console.log("ERROR :", error);
                    }
                });
            });

            //Validate Input
            $('#depid, #comid, #depcode,#depname').on('keyup', function(){
                $('.submit-btn').attr("disabled", function(){
                   if(  
                        $('#depid').val() === '' || 
                        $('#comid').val() === '' || 
                        $('#depcode').val()     === '' || 
                        $('#depname').val()   === '' 
                    )   return true;
                    return false;
                });
                
            });
        });

    </script>
@endsection
