@extends('headbar')

@section('content')
    <form method = "post" action="/trialBalance/report">
    {{csrf_field()}}
        <div class="content-form">
            <div class="container">
                <div class="row">
                    <h3>Trial Balance</h3>
                </div>
                <div class="row">
                    <div class="form-group">
                    <label for="cpn">period:</label>
                    <input type="date" name="start">
                    to
                    <input type="date" name="end">
                </div>
                <button class="btn">Click</button>
                <br>
            </div>        
        </div>
    </form>
@endsection
